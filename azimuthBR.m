%Main script of AZIMUTH implementation.
%
% SYNOPSIS:
%   azimuthBR
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azimuthBR
%
% SEE ALSO:
%   startup, azWellcome
% 

% install subdirectories
startup

% wellcome window (main)
azMain()