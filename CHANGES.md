0.1.4:
------
Release date: **2015/04/19**
 * Merge branch 'release/0.1.4'
 * remove unittest folder
 * Merge branch 'feature/coreAnalysis' into develop
 * added box plot function
 * removed pie chart in core analysis
 * added bar plot method
 * added delete core parameter method
 * impl. rename parameters into core data
 * init core module implementation
 * initial implementation Core Analysis module
 * Merge branch 'feature/markers' into develop
 * fixes conflicts
 * impl. delete and rename markers methods
 * little change to improve performances
 * added load markers method
 * directory restructuring
 * remove yellow collor and remove deleted plot in CrossPlot module
 * Merge branch 'release/0.1.2' into develop
 * Merge branch 'release/0.1.2'
 * Merge branch 'release/0.1.1'
 * Merge branch 'release/0.1.0'

0.1.2:
------
Release date: **2015/02/11**
 * remove grids and frequencies labels on rose plots & fixes on add buttons Save... and exit/return in well project build/edit
 * Merge branch 'feature/WellLogViewer-tadpole' into develop
 * Impl. select and plot tadpole graphs
 * Initial impl tadpole
 * fix bugs on xy-plots, well delete and save field routines
 * fixes on dialog window title
 * Merge branch 'release/0.1.1' into develop

0.1.1:
------
Release date: **2015/02/07**
 * clean some rose and crossplot test into unittest
 * Merge branch 'feature/rose_plot' into develop
 * Selection feature impl in rose plot
 * include autosave files(*.asv) and add well and fracture logs name on legend in rose plots
 * Impl of single and multiples rose plots
 * Merge branch 'release/0.1.0' into develop

0.1.0
------
Release date: **2015/02/04**
 * Merge branch 'feature/feature-WAM-XYPlots' into develop
 * Fixes on delete plot & organize, check some help header
 * Impl. add, Select and add more plots
 * Test of Impl. & force uigetfile to work
 * Organize folders and adding some Gabriel's suggestions001 (see GabrielSuggestions001.pdf)
 * Initialise azimuth project
 * Initial commit

