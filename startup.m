function startup
%Amend MATLAB PATH to handle AZIMUTH implementation.
%
% SYNOPSIS:
%   startup
%
% PARAMETERS:
%   None.
% 
% RETURNS:
%   current subdirectories amend to MATLAB PATH 
%
% USAGE:
%   >> startup
%
% SEE ALSO:
%   fileparts, mfilename, genpath, split_path
% 


   d = fileparts(mfilename('fullpath'));

   p = split_path(genpath(d));

   addpath(p{:});

end

%--------------------------------------------------------------------------

function p = split_path(p)
%split path string into all the subdirectories (including empty subdirectories).
%
% SYNOPSIS:
%   p = split_path(dir)
%
% PARAMETERS:
%   dir  -  path string starting in dir, , plus, recursively, all the 
%           subdirectories of dir, including empty subdirectories.
%
% RETURNS:
%   p    -  cell array contining a list of subdirectories.
%
% USAGE:
%   >> p = split_path(cd)
%
% SEE ALSO:
%    path, addpath,regexp, try, pathsep, strsplit, genpath 
% 

   try
      p = regexp(p, pathsep, 'split');
   catch  %#ok
      % Octave compatibility.  It is an error to get here in an M run.
      p = strsplit(p, pathsep);
   end
end

%--------------------------------------------------------------------------
