function plotFaultHorizon(Faults, W)
%Function to display Fault horizon with Wells (whether Well structure supplied)
%
% SYNOPSIS:
%   plotFaultHorizon(Faults, W)
%   plotFaultHorizon(Faults)
%
% PARAMETERS:
%   Faults  - Fault structure mostly as detailed in fault_structure.
%   W       - Wells strucuture mostly as detailed in azvar_structure.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> plotFaultHorizon(Faults, W); %plot faults and well on map view
%   >> plotFaultHorizon(Faults); %plot faults on map view
%
% SEE ALSO:
%   menu, plot, text, fault_structure, azvar_structure
% 
%

if (nargin>1)
    withWells=1;
else
    withWells=0;
end


% 2D 
figure

n=length(Faults.type);
hold on
for i=1:n
    idx=find(Faults.type==i-1);
    if (~isempty(idx))
        xfault=Faults.X(idx); yfault=Faults.Y(idx);
        plot(xfault,yfault,'-k');
    end
end
 xlabel('X'); ylabel('Y');
 
 
if (~withWells)
    title('Fault Horizon');
else
    title('Fault Horizon and wells');
%     hold on
    for i=1:W.azField.nWells
        plot(W.azField.Wells(i).X(1),W.azField.Wells(i).Y(1),...
            'or','LineWidth',6);
        text(W.azField.Wells(i).X(1), W.azField.Wells(i).Y(1),...
            W.azField.Wells(i).Name,'FontSize',18);
    end
    hold off
end
     
% % 3D
% figure
% hold on
% for i=1:n
%     idx=find(Faults.type==i-1);
%     if (~isempty(idx))
%         xfault=Faults.X(idx); yfault=Faults.Y(idx); zfault=Faults.Z(idx);
%         plot3(xfault, yfault, zfault,'.')
%     end
% end
% hold off

pause

end

