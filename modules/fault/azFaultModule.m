function azFaultModule()
%Driver function of Fault module.
%
% SYNOPSIS:
%   azFaultModule
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azFaultModule
%
% SEE ALSO:
%   menu, azWellAnalysisMod, switch
% 
% 

clc;
FaultModWellcome = 'Azimuth::Fault Module';

azwell=0; withWells=0;
while(azwell<5)
    azwell=menu(FaultModWellcome,...
        'Load Map of faults',...            % azwell == 1
        'Save Map of faults',...            % azwell == 2
        'Load Project from fracModule',...  % azwell == 3
        'Plot horizon (2D graph)',...       % azwell == 4
        'Exit');                            % azwell == 5
    
    switch azwell
        case 1
            azFault = load_Faults();
        case 2            
            azSaveField('Faults',azFault);
        case 3
            W = azLoadField('Load fracModule Project');
            withWells=1;
        case 4
            if (withWells)
                plotFaultHorizon(azFault,W)
            else
                plotFaultHorizon(azFault)
            end
        case 5
            return
    end

end

end