function [ faults] = load_Faults
%Function to load fault maps structure from file data.
%
% SYNOPSIS:
%   F = load_Faults()
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   F   - structure used in the fault module to process some computation.
%         see fault_structure.
%
% USAGE:
%   >> F = load_Faults()
%
% SEE ALSO:
%   menu, load, struct, fault_structure
% 
%

[filename,pathname]=azGetFile('*.*');
    
[xfa, yfa, zfa, tfa] = textread(strcat(pathname,filesep,filename),'%f%f%f%f');


faults = struct('X', xfa,...
                'Y', yfa,...
                'Z', zfa,... 
                'type', tfa);

end

