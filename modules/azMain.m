function azMain()
%Azimuth program wellcome window (display first window of azimuth program)
%
% SYNOPSIS:
%   azMain
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azMain
%
% See also:
%   menu, azFaultModule, azWellAnalysisMod, azCore, switch
% 
% 
%
clc
wellcomeMsg = 'Azimuth::Load Module';
azwell=0;
while(azwell<4)
    azwell=menu(wellcomeMsg,...
        'Core Analysis',... % azwell == 1
        'Well Analysis',... % azwell == 2
        'Seismic',...       % azwell == 3
        'Exit');            % azwell == 4
    
    switch azwell
        case 1
            azCore();
        case 2
            azWellAnalysisMod();
        case 3
            azFaultModule            
        case 4
            return
    end

end




end

