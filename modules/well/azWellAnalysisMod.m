function azWellAnalysisMod
%Driver function of well analysis module.
%
% SYNOPSIS:
%   azWellAnalysisMod()
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azWellAnalysisMod()
%
% SEE ALSO:
%   menu, azWellProject, switch, azLoadField, azAnalysis
% 
%

clc; clear all;
wellcomeMsg = 'Well Analysis Module';

azwell=0; azField = [];
while(azwell<3)
    azwell=menu(wellcomeMsg,...
        'Project',...       % azwell == 1
        'Analysis',...      % azwell == 2
        'Exit');            % azwell == 3
    
    switch azwell
        case 1
            [azField]=azWellProject();
        case 2
            if isempty(azField)
                azField = azLoadField('Load Project to start analysis');
            end
            azAnalysis(azField);
        case 3
            return
    end

end

