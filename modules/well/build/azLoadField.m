function ldField = azLoadField(title_)
%wrap function to load a derived field during runtime (i.e. Load variable 
% from *.mat files - project's variable that contains a bunch of variables).
%
% SYNOPSIS:
%   ldField = azLoadField(title_)
%
% PARAMETERS:
%   title_  - string to display on window that will be opened during pick
%             process of file.
%
% RETURNS:
%   ldField - structure used in the azimuth program. see azvar_structure.
%
% USAGE:
%   >> ldField = azLoadField(title_)
%
% SEE ALSO:
%   azGetFile, load, matfile, azvar_structure
% 

[wfile,wpath]=azGetFile('*,mat',title_);
ldField=load(strcat(wpath,filesep,wfile));
ldField=getfield(ldField,'azField'); %#ok<GFLD>
end

