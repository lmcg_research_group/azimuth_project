function newSWells = azLoadFractures(SWells)
%Add/Load fracture logs into well structure (see well_structure.m).
% 
% SYNOPSIS:
%   newSWells = azLoadFractures(SWells
%
% PARAMETERS:
%   SWells    - well structure to be changed. see 
%
% RETURNS:
%   newSWells - new well structure. 
%
% USAGE:
%   >> newSWells = azLoadFractures(SWells
%
% SEE ALSO:
%   struct, azvar_structure, listdlg, azGetFile, inputdlg, interp1, fprintf


    %% check input
    assert(isvector(SWells),'*ERROR: input variable must be a vector of structures');
    nWells = numel(SWells);
    for iwell=1:nWells
        assert(isstruct(SWells(iwell)),...
            '*ERROR: all components of input must be a structure type')
    end
    
    % copy input to output
    newSWells = SWells;
    
    
    %% choose well structure to open/change
    % collecting name of wells
    wNames = cell(nWells,1);
    for iwell=1:nWells
        wNames{iwell} = SWells(iwell).Name;
    end
    
    % Open list dialog window to select well 
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 
	
    [iwell,ok] = listdlg('PromptString','Select well to add fracture log:',...
                    'SelectionMode','single',...
                    'ListString',wNames);

	% user click Cancel or Close dialog box... so we leave here
    if (ok==0), return; end
    

    %% read fracture log data
    % If we got here, we will add fracture log
    
    stat=1;  % =1,user click Cancel or close windows
    while(stat~=0) % oo loop until user select a fracture log
        [filename,pathname,fid,stat]=azGetFile('*.*', ...
            sprintf('Select fracture log to well: %s',SWells(iwell).Name),...
            cd, 'on');; %#ok
    end
       
    % check if one file or multiples files are selected. 
    if (iscell(filename))
        nF = numel(filename);
    else
        nF = 1; filename = {filename};
    end
    
    % loop over fracture logs
    for ifac=1:nF        
        
        [data,~]=textscan(fid(ifac),'%s %s %s %s');%
        isAttributed=0;
        if (length(data)~=4) 
            error('Number of column into fracture file not compatible (must be 4)')
        else
            % file preview 
            frewind(fid(ifac));
            filePreview(filename{ifac},fid(ifac),1,5)
            fclose(fid(ifac));
            
            % store data
            while (isAttributed==0)
                prompt={'Mensured Depth (MD):',...
                        'Category:',...
                        'DipAzimuth:',...
                        'Dip:'};
                dlg_title='Column attribute for input data';
                numlines=1;
                def_ans = {'1','2','3','4'};
                answer=inputdlg(prompt,dlg_title,numlines,def_ans);
                % Cancel procedure... so we get out here
                if isempty(answer), return; end
                
                % Check if any input is empty
                if (isempty(strtrim(answer{1}))), continue; end
                if (isempty(strtrim(answer{2}))), continue; end
                if (isempty(strtrim(answer{3}))), continue; end
                if (isempty(strtrim(answer{4}))), continue; end
                
                % convert to numeric variables & assigns values
                idMD=str2num(answer{1}); %#ok
                idCT=str2num(answer{2}); %#ok
                idAZ=str2num(answer{3}); %#ok
                idDP=str2num(answer{4}); %#ok
                if (length(intersect(1:4,[idMD,idCT,idAZ,idDP]))~=4)
                    msgdlg('Incompatible column attribute','Column Attribute',...
                        'warn');
                else
                    isAttributed=1;
                end
            end
        end

        ndata = length(data{1})-1; % remove header
        MD = zeros(ndata,1); AZIMUTH = zeros(ndata,1);  %#ok<NASGU>
        DIP = zeros(ndata,1); X  = zeros(ndata,1);      %#ok<NASGU>
        Y  = zeros(ndata,1); Z  = zeros(ndata,1);       %#ok<NASGU>

        MD = str2double(data{1,idMD}(2:end));
        AZIMUTH = str2double(data{1,idAZ}(2:end));
        DIP = str2double(data{1,idDP}(2:end));    
        bedding = data{1,idCT}(2:end);

        % removing NAN and setting zero
        i=isnan(MD); MD(i)=0.0; 
        i=isnan(AZIMUTH); AZIMUTH(i)=0.0; 
        i=isnan(DIP); DIP(i)=0.0; 
        
        % check input data
        % 0 <= DipAzimuth <= 360
        idx0 = find(AZIMUTH<0); 
        assert(isempty(idx0),...
            sprintf('\n**ERROR: DipAzimuth<0 in the line %u',idx0+1))
        idx0 = find(AZIMUTH>360); 
        assert(isempty(idx0),...
            sprintf('\n**ERROR: DipAzimuth>360 in the line %u',idx0+1))
        
        % 0<= Dip <=90
        idx0 = find(DIP<0); 
        assert(isempty(idx0),...
            sprintf('\n**ERROR: Dip<0 in the line %u',idx0+1))
        idx0 = find(DIP>90); 
        assert(isempty(idx0),...
            sprintf('\n**ERROR: Dip>90 in the line %u',idx0+1))
        
        % interp. of X, Y, Z coordinates along MD
        X = interp1( SWells(iwell).MD, SWells(iwell).X, MD, 'spline');
        Y = interp1( SWells(iwell).MD, SWells(iwell).Y, MD, 'spline');
        if (~isempty(SWells(iwell).Z))
            Z = interp1( SWells(iwell).MD, SWells(iwell).Z, MD, 'spline');
        else
            Z = [];
        end

        [pw,FLogName,e]=fileparts(filename{ifac}); %#ok
        % new fracture log
        FracField(ifac) = struct('Name', FLogName,...
                                 'Az'  , AZIMUTH ,...
                                 'Dip' , DIP     ,...
                                 'MD'  , MD      ,...
                                 'X'   , X       ,...
                                 'Y'   , Y       ,...
                                 'Z'   , Z       ,...
                                 'Type', char(bedding)); %#ok<AGROW>

    end


    %% add fractures log to well
    
    % check if well has fracture logs
    nFLogs=0;
    if (isfield(SWells(iwell),'FracturesLogs'))
        if (~isempty(SWells(iwell).FracturesLogs)) 
            % well has fracture logs
            nFLogs = numel(SWells(iwell).FracturesLogs);
            FNames=cell(nFLogs,1);
            for i=1:nFLogs
                FNames{i}=newSWells(iwell).FracturesLogs(i).Name;
            end
        end
    end
    
    % add Fractures logs
    if (nFLogs) % there is Fractures logs in this well
        ifac=0;
        for i=1:nF
            % check if log already exist into well
            idx=find(strcmpi(FNames,FracField(i).Name), 1);
            if (isempty(idx))
                ifac=ifac+1;
                newSWells(iwell).FracturesLogs(nFLogs+ifac) = FracField(i);
            else
               fprintf('*WARNNING: log %s already added... skiping\n',...
                   FracField(i).Name)
            end
        end
    else % there isn't Fractures logs in this well
        newSWells(iwell).FracturesLogs = FracField;
    end
    
end


