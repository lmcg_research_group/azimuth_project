function [azField, status] = azWellNewProject(inproj)
%Function create Projects on azimuth program and build well structure.
% (see azvar_structure).
%
% SYNOPSIS:
%   [azField, status] = azWellNewProject();
%   azField = azWellNewProject()
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   azField - azvar structure used in the azimuth program. see azvar_structure.
%   stat  - An integer that identifies the status of this process. If some
%           erroneous behavior happen, stat is 1.
%
% USAGE:
%   >> [azField, status] = azWellNewProject();
%   >> azField = azWellNewProject()
%
% SEE ALSO:
%   menu, struct, azvar_structure, azLoadWell, azDeleteWell, azRenameWell,
%   azLoadFractures, azDeleteFractures, azRenameFractures, printWellsTree

%% Check inputs
if (nargin==1)
    wellcomeMsg = 'edit Azimuth Project: building data';
    nWells=inproj.nWells; Wells=inproj.Wells; M = [];
    if (isfield(inproj,'Markers'))
        M = inproj.Markers;
    end
else
    wellcomeMsg = 'new Azimuth Project: building data';
    nWells=0; Wells =[]; M = [];
end

%% Choose procedures
azField = [];
aznewPrj=0;
while(aznewPrj<12)
    % main menu of new project build
    aznewPrj=menu( wellcomeMsg       ,...
                   'add Wells'       ,...    % aznewPrj =  1
                   'remove Wells'    ,...    % aznewPrj =  2
                   'rename Wells'    ,...    % aznewPrj =  3
                   'add Fractures'   ,...    % aznewPrj =  4
                   'remove Fractures',...    % aznewPrj =  5
                   'rename Fractures',...    % aznewPrj =  6
                   'add Markers'     ,...    % aznewPrj =  7
                   'remove Markers'  ,...    % aznewPrj =  8
                   'rename Markers'  ,...    % aznewPrj =  9
                   'Print Project Tree'  ,...% aznewPrj = 10
                   'Save ...',...            % aznewPrj = 11
                   'Exit/Return');           % aznewPrj = 12

    switch(aznewPrj)
        %------------------------------------------------------------------
        case (1)  % add Wells
            [W,stat]=azLoadWell();
            if (stat==0)
                if (nWells==0)
                    Wells=W; nWells = numel(Wells);
                else
                    Wells=[Wells, W]; nWells = nWells+numel(W); %#ok<*AGROW>
                end
                for i=1:numel(W)
                    fprintf('*Added well %s\n', W(i).Name);
                end
            end
        %------------------------------------------------------------------
        case (2)  % del Wells
            Wells2 =azDeleteWell(Wells);
            clear Wells;
            Wells=Wells2; clear Wells2;
            nWells = length(Wells);

        %------------------------------------------------------------------
        case (3)  % rename Wells
            Wells = azRenameWell(Wells);

        %------------------------------------------------------------------
        case (4)  % Load fracture logs
            [Wells] = azLoadFractures(Wells);

        %------------------------------------------------------------------
        case (5)  % Delete fracture logs
            [Wells] = azDeleteFractures(Wells);

        %------------------------------------------------------------------
        case (6)  % Load fracture logs
            [Wells] = azRenameFractures(Wells);

        %------------------------------------------------------------------
        case (7)  % add markers logs
            if (isempty(Wells))
                fprintf('*WARNNING: You Should be add Wells before add marker... skiping\n');
            else
                tmpW = azLoadMarkers(Wells);
                clear Wells
                Wells = tmpW;
            end
        %------------------------------------------------------------------
        case (8)  % Remove markers logs
            tmpW = azDeleteMarkers(Wells);
            clear Wells
            Wells = tmpW;

        %------------------------------------------------------------------
        case (9)  % rename markers logs
            [tmpW] = azRenameMarkers(M);
            clear M
            M = tmpW;

        %------------------------------------------------------------------
        case (10)
            if (nargin==1)
                printWellsTree(Wells, inproj.Name)
            else
                printWellsTree(Wells)
            end

        %------------------------------------------------------------------
        case (11)
            if (~isempty(Wells))
                if (~isempty(M))
                    azField=azSaveField('Wells',Wells,'nWells',nWells,...
                        'Markers',M);
                else
                    azField=azSaveField('Wells',Wells,'nWells',nWells);
                end
                printReport(azField)
            else
                azField = [];
            end
    end
end
status=0;
end



function printWellsTree(Field,Name)

if (nargin>1)
    NameProj=Name;
else
    NameProj='Unamed';
end


fprintf('\n\n\n-------------------\n');
fprintf('Azimuth Project info\n');
fprintf('Name: %s\n', NameProj);
fprintf('nWells: %d\n',numel(Field));
fprintf('Wells List:\n')

for w=1:numel(Field)
   fprintf('- well#%d::Name: %s\n',w, Field(w).Name)
   if (isfield(Field(w),'FracturesLogs'))
       for f=1:numel(Field(w).FracturesLogs)
           fprintf(' -> FractureLog%d::Name: %s\n', f, ...
               Field(w).FracturesLogs(f).Name)
       end
   end
end

if (isfield(Field,'Markers'))
    fprintf('\n\nMarker List:\n');
    for m=1:numel(Field.Markers)
       fprintf('- Marker#%d::Tag: %s \n',m, Field.Markers(m).Name);
    end
end

end




function printReport(Field)
clc;
fprintf('\n\n\n-------------------\n');
fprintf('Azimuth Project info\n');
fprintf('Name: %s\n',Field.Name);
fprintf('nWells: %d\n',Field.nWells);
fprintf('Wells List:\n');

for w=1:Field.nWells
   fprintf('- well#%d::Name: %s\n',w, Field.Wells(w).Name);
   if (isfield(Field.Wells(w),'FracturesLogs'))
       for f=1:numel(Field.Wells(w).FracturesLogs)
       fprintf(' -> FractureLog%d::Name: %s\n', f, ...
           Field.Wells(w).FracturesLogs(f).Name);
       end
   end
end

if (isfield(Field,'Markers'))
    fprintf('\n\nMarker List:\n');
    for m=1:numel(Field.Markers)
       fprintf('- Marker#%d::Tag: %s \n',m, Field.Markers(m).Name);
    end
end

end

