function [nW, stat] = azLoadMarkers(W)
%Function to add marker data from file.
% 
% SYNOPSIS:
%   [nW,stat] = azLoadMarkers;
%   nW = azLoadMarkers;
%
% PARAMETERS:
%  W      - well structure used in the well analysis module to process some 
%         computation. see well_structure.
%
% RETURNS:
%   nW    - well structure modified after added some markers.
%   stat  - An integer that identifies the status of this process. If some
%           erroneous behavior happen, stat is 1.
%
% USAGE:
%   >> nW = azLoadMarkers(W);
%
% SEE ALSO:
%   menu, struct, well_structure, azGetFile, fileparts, fprintf
% 

    %% check inputs
    assert(isvector(W),'*ERROR: input variable must be a vector of structures');
    nWells = numel(W);
    for iwell=1:nWells
        assert(isstruct(W(iwell)),...
            '*ERROR: all components of input must be a structure type')
    end

    %% Opening marker file
    % Open dialog window to select file
    [filename,~,fid,stat]=azGetFile('*.*','Select Marker file',[ ],'on');
    
    % check if file is good (see help azGetFile).
    if (stat==1), M=[]; return; end; %#ok<NASGU>
        
    % opened one file or multiples files
    if (iscell(filename))
        nM = numel(filename);
    else
        nM = 1; filename = {filename};
    end
    
    %% Reading markers
    % loop over markers
    for imark=1:nM
        % extract fullpath, nameOfFile and extension
        [pm,mName,e]=fileparts(filename{imark}); %#ok 
        
        % read data into file
        fprintf('*Reading file: %s ...\n',filename{imark});
        dat = textscan(fid(imark),'%s %s %s');
        
        % Construct Marker structure
        Well = dat{1}; MD = dat{2}; Names = dat{3};
        mNames = unique(Names);
        for i = 1: numel(mNames)
            M(i).Name = mNames{i}; %#ok<*AGROW>
            wellCount = 0;
            for j = 1: numel(Names)
                if strcmp(Names{j},M(i).Name)
                    wellCount = wellCount + 1;
                    M(i).Well(wellCount) = Well(j);
                    M(i).MD(wellCount) = str2double(MD{j});
                end
            end
        end
    end
    
    %% add marker
    nW = W; % copy input to output
    for j = 1: numel(mNames)                % loop over markers
        for k = 1 : length(M(j).Well)       % loop over wells marked by k-th marker
            for i = 1 : nWells                          % loop over wells
                if (strcmpi(M(j).Well(k),nW(i).Name)) % i-th wells will be marked by j-th marker
                    % check if well has markers
                    if (isfield(nW(i),'Marker') && ~isempty(nW(i).Marker))
                        % Was i-th wells already marked by j-th marker?
                        hasMarker = false; mrks={nW(i).Marker(:).Name};
                        for m = 1 : numel(mrks)
                            if (strcmpi(M(j).Name, mrks{m}))
                                hasMarker = true; break;
                            end
                        end
                        if (~hasMarker) % not found j-th marker in i-th wells... so is new marker!!
                            % i-th wells was already marked by j-th marker
                            nM = length(nW(i).Marker);
                            nW(i).Marker(nM+1).Name = M(j).Name;
                            nW(i).Marker(nM+1).MD = M(j).MD(k);
                            fprintf('*Added %s marker in well %s\n', ...
                                M(j).Name,nW(i).Name);
                        else
                            fprintf('*WARNNING: marker %s already added in well %s... skiping\n',...
                                M(j).Name,nW(i).Name);
                        end
                    else
                        % first marker added in i-th well
                        nW(i).Marker(1).Name = M(j).Name;
                        nW(i).Marker(1).MD = M(j).MD(k);
                        fprintf('*Added %s marker in well %s\n', ...
                            M(j).Name,nW(i).Name);
                    end
                    break;
                end
            end
        end
    end
end


