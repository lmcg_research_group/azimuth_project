function nW = azRenameMarkers(W)
%Function to rename markers from well structure as described in well_structure.
%
% SYNOPSIS:
%   nW = azRenameMarker(W)
%
% PARAMETERS:
%	W     - well structure used in the well analysis module to process some 
%         computation. see well_structure.
%
% RETURNS:
%   nW    - well structure modified after rename some marker.
%
% USAGE:
%   >> nW= azRenameMarker(WOld);
%
% SEE ALSO:
%   menu, struct, well_structure, listdlg, inputdlg
% 

    %% check input
    assert(isvector(W),'*ERROR: input variable must be a vector of structures');
    for i=1:numel(W)
        assert(isstruct(W(i)),...
            '*ERROR: all components of input must be a structure type')
    end
    
    
    %% choose marker(s) to delete
    
    % collecting markers
    mNames = []; m = 0; %#ok<*AGROW>
    for i=1:numel(W)                            % loop over wells
        if (isempty(mNames))                    % special case first well
            for j=1:numel(W(i).Marker)          % add all markers!
                m = m + 1; mNames{m} = W(i).Marker(j).Name;
            end
        else
            for j=1:numel(W(i).Marker)          % loop over marker on i-th well
                hasMarker = false;              % check whether marker already added into mNames!
                for k=1:numel(mNames)
                    if (strcmpi(W(i).Marker(j).Name, mNames{k})) % Marker was already added into mNames
                        hasMarker = true; break;
                    end
                end
                if (~hasMarker)
                    m = m + 1; mNames{m} = W(i).Marker(j).Name; % adds markers into mNames
                end
            end            
        end
    end
    
    % copy input to output
    nW = W;
    
    % List dialog window to select well 
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 
	
    [i,ok] = listdlg('PromptString','Select marker to rename:',...
                    'SelectionMode','multiple',...
                    'ListString',mNames);

	% user click Cancel or Close dialog box... so we leave here
    if (ok==0); return;  end
    
    %% rename marker list data
    
    % loop over marker selected
    for im=1:numel(i)
        prompt={'New Marker Name:'};
        dlg_title='Rename Marker';
        numlines=1;
        def_ans = {mNames{i(im)}};
        answer=inputdlg(prompt,dlg_title,numlines,def_ans);
        
        % Cancel procedure... so we get out here
        if isempty(answer), continue; end
    
        % If we get here, we will rename marker
        Mrk = mNames{i(im)};
        for k=1:numel(nW)                       % loop over wells
            for j=1:numel(nW(k).Marker)         % loop over marker on i-th well
                % check whether i-th well has marker
                if (strcmpi(nW(k).Marker(j).Name, Mrk))
                    nW(k).Marker(j).Name = answer{1};  % rename it!
                    break;
                end
            end            
        end
    end
end