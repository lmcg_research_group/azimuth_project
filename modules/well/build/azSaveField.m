function azField=azSaveField(varargin)
%wrap function to save derived field during run time from a variable 
% or expression(i.e. Save variable to *.mat files - project's variable that 
% contains a bunch of variables).
%
% SYNOPSIS:
%   ldField = azSaveField('pn1', pv1, ...)
%
% PARAMETERS:
%    'pn1'/pv1 - List of property names/property values. This list will be
%               passed directly on to save variable.
%
% RETURNS:
%   azField - structure saved in the azimuth program. see azvar_structure.
%
% USAGE:
%   >> prm = azSaveField('foo', 1, 'bar', pi, 'baz', true)
%
% SEE ALSO:
%   assert, uiputfile, fileparts, save, azvar_structure, struct, fieldname
% 

if (mod(numel(varargin), 2) == 0)
    assert (iscellstr(varargin(1 : 2 : end)), ...
           'input parameters should be ''key''/value pairs.');
else
    error('input parameters should be ''key''/value pairs.')
end

% In this way we force (brutally!!) the uigetfile works
file = 0;
while (isnumeric(file))
    [file, path_] = uiputfile('Unnamed.mat','Save Azimuth Project as...');
end
[pw,pName,e]=fileparts(file); %#ok
     
azField.Name=pName;
for ifield=1:2:length(varargin)
    azField.(varargin{ifield}) = varargin{ifield+1};
end


fprintf('* saving to Azimuth Project %s into %s\n',pName, path_);
save(strcat(path_,filesep,file),'azField');

end
