function proj = azWellProject
%AZWELLPROJECT Function to create/load/edit projects of well analysis module.
%
% SYNOPSIS:
%   prj = azWellProject()
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   prj  - project variable, containing wells data, fractures logs and
%          markers logs;
%
% USAGE:
%   >> wellProj = azWellProject()
%
% SEE ALSO:
%   menu, switch, azWellNewProject, azLoadField
% 
%

clc; clear all;
wellcomeMsg = 'Well Analysis Project Builder';

azwell=0; proj = [];
while(azwell<4)
    azwell=menu(wellcomeMsg,...
        'New Project',...       % azwell == 1
        'Load Project',...      % azwell == 2
        'Edit Project',...      % azwell == 3
        'Exit/Return');         % azwell == 4
    
    switch azwell
        case 1 % new project creation
            proj = azWellNewProject();
        case 2 % load project
            proj = azLoadField('Load Project');
        case 3 % edit project 
            if isempty(proj)
                proj = azLoadField('Load Project to edit');
            end
            proj = azWellNewProject(proj);
        case 4
            return
    end

end

