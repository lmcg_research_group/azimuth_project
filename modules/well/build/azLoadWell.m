function [W,stat] = azLoadWell( )
%Function to load well data from file and build well structure (see azvar_structure.m).
% 
% File format suported:
%   -> PETREL;
%   -> HAZI;
%   -> SiGeo formats 
% Pattern to identify formats:
%   o PETREL Format: NAMEWELL.dev 
%   o HZI Format profile data containing MD, HAZI, SDEV: NAMEWELL.hzi 
%   o SiGeo Format containing MD, DX, DY data: NAMEWELL.mxy
%   o SiGeo Format containing X, Y, Z data: NAMEWELL.xyz 
% 
% where: NAMEWELL is a name that will be display to identify it during
% plots
%
% SYNOPSIS:
%   [W,stat] = azLoadWell( );
%   W = azLoadWell( )
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   W     - well structure used in the well analysis module to process some 
%         computation. see azvar_structure.
%   stat  - An integer that identifies the status of this process. If some
%           erroneous behavior happen, stat is 1.
%
% USAGE:
%   >> W= azLoadWell( );
%
% SEE ALSO:
%   menu, struct, azvar_structure, azGetFile, fileparts, fprintf
% 

    % Open dialog window to select file
    [filename,~,fid,stat]=azGetFile('*.*',[ ],[ ],'on');
    
    % check if file is good (see help azGetFile).
    if (stat==1), W=[]; return; end;
    
    % opened one file or multiples files
    if (iscell(filename))
        nW = numel(filename);
    else
        nW = 1; filename = {filename};
    end
    
    % loop over wells
    for iwell=1:nW
        % extract fullpath, nameOfFile and extension
        [pw,wName,e]=fileparts(filename{iwell}); %#ok 
        
        fprintf('*Reading file: %s ...',filename{iwell})
        % switch between reader (suported formats)
        switch(e)
            case ('.dev')    % file extension is .dev
                DEV = readDEV(fid(iwell));
                kelly = DEV.KB; wmd = DEV.MD; wx = DEV.X; 
                wy    = DEV.Y;  wz  = DEV.Z; wtvd= DEV.TVD;
                clear DEV % clear DEV variable
            case ('.mxy')    % file extension is .mxy
                MXY=readMXY(fid(iwell));
                kelly = [ ]; wmd = MXY.MD; wx = MXY.X; 
                wy    = MXY.Y;  wz  = [ ]; wtvd= MXY.TVD;
                clear MXY % clear HZI variable
            case ('.xyz')    % file extension is .xyz
                XYZ=readXYZ(fid(iwell));
                kelly = []; wmd = []; wx = XYZ.X; 
                wy    = XYZ.Y;  wz  = XYZ.Z; wtvd= [];
                clear XYZ % clear XYZ variable
    %         case ('.hzi')    % file extension is .hzi
    %             HZI=readHZI(fid(iwell)); 
    %             kelly = HZI.KB; wmd = HZI.MD; wx = HZI.X; 
    %             wy    = HZI.Y;  wz  = HZI.Z; wtvd= HZI.TVD;
    %             clear HZI % clear HZI variable
            otherwise
                exception = MException('AZError:UnsupportedFiles',...
                    'File extension not supported ''%s',e);
                throw(exception);
        end
        
        % Construct Well structure
        W(iwell) = struct('Name', wName,...
               'KB' , kelly,...
               'MD' , wmd  ,...
               'X'  , wx   ,...               
               'Y'  , wy   ,...               
               'Z'  , wz   ,...
               'TVD', wtvd );

        % close access of file
        fclose(fid(iwell));
        fprintf(' Done\n')
    end
end


