%main variable structure used in the well module.
%
%   azField - representation of project variables created during some 
%   analysis process.  A master structure having the following fields:
%    - Name --
%        Name of project loaded.
%
%    - Wells --
%        A structure specifying properties for each individual well in the
%        project. See WELLS below for details.
%
%    - nWells --
%        Number of wells loaded on current project.
% 
% 
%%
%   WELLS - well data structure imported from files.
%    - Name --
%        Well name (name of file from which well data was imported).
% 
%    - KB --
%       Kelly Bushing value
% 
%    - X --
%       X-coordinate of well path
% 
%    - Y --
%       Y-coordinate of well path
% 
%    - Z --
%       Z-coordinate of well path
% 
%    - MD --
%       Measured Depth values of well path
% 
%    - TVD --
%       True Vertical Depth of well path
% 
%    - FracturesLogs --
%       A structure specifying properties for Fracture Logs measured in some
%       depth along well path. See FRACTURESLOGS below for details.
%    - Markers --
%       A structure specifying properties for Markers. See MARKERS below
%       for details.
% 
%%
% 
%   FRACTURESLOGS Fracture Logs structure imported from some fracture log 
%       files:
% 
%   - Name --
%       String containing the log file name. 
% 
%   - AZ --
%       DipAzimuth attribute.
% 
%   - DIP --
%       Dip attribute.
% 
%   - MD --
%       Measured Depth.
% 
%   - X  --
%       x-coordinate of fracture set.
% 
%   - Y  --
%       y-coordinate of fracture set.
% 
%   - Z  --
%       z-coordinate of fracture set.
% 
%   - Type  --
%       Geological categories (or entities) observed along of some section of well 
%
%%
% 
%   MARKERS Markers structure imported from some marker log files:
% 
%   - Name --
%       String containing the Marker name. 
% 
% 
%   - MD --
%       Measured Depth.
% 