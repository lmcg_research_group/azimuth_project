% Files
%   azWellProject       - Function to create/load/edit projects of well analysis module.
%   azLoadField         - wrap function to load a derived field during runtime (i.e. Load variable 
%   azSaveField         - wrap function to save derived field during run time from a variable 
%   azDeleteFractures   - Delete fractures data structure into wells struct.
%   azDeleteWell        - Function to delete wells from well structure as described in well_structure.
%   azLoadFractures     - Add/Load fracture logs into well structure (see well_structure.m).
%   azLoadMarkers       - Add/Load markers logs. (NOT IMPLEMENTED)
%   azLoadWell          - Function to load well data from file and build well structure (see azvar_structure.m).
%   azRenameFractures   - Rename fractures logs.
%   azRenameWell        - Function to rename wells from well structure as described in well_structure.
%   azWellNewProject    - Function create Projects on azimuth program and build well structure.
%   well_structure    - main variable structure used in the well module.
