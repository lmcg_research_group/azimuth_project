function newSWells = azRenameFractures(SWells)
%Rename fractures logs.
% 
% SYNOPSIS:
%   newSWells = azRenameFractures(SWells)
%
% PARAMETERS:
%   SWells    - well structure to be changed. see 
%
% RETURNS:
%   newSWells - new well structure. 
%
% USAGE:
%   >> WellsNew = azRenameFractures(Wells)
%
% SEE ALSO:
%   fgets, sscanf, struct, assert, listdlg, inputdlg


    %% check input
    assert(isvector(SWells),'*ERROR: input variable must be a vector of structures');
    nWells = numel(SWells);
    for iwell=1:nWells
        assert(isstruct(SWells(iwell)),...
            '*ERROR: all components of input must be a structure type')
    end
      
    %% choose well structure to open/change
    % collecting name of wells
    wNames = cell(nWells,1);
    for iwell=1:nWells
        wNames{iwell} = SWells(iwell).Name;
    end
    
    % Open list dialog window to select well 
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 
	
    [iwell,ok] = listdlg('PromptString','Select well data:',...
                    'SelectionMode','multiple',...
                    'ListString',wNames);

	% user click Cancel or Close dialog box... so we leave here
    if (ok==0)
        % copy input to output
        newSWells = SWells;
        return; 
    end
    

    %% choose to delete fracture log data
    % If we got here, we will add fracture log
    newSWells = SWells;
    for iw=1:numel(iwell)
        w = iwell(iw);
        nLogs = numel(newSWells(w).FracturesLogs);
        
        % collecting name of wells
        wNames = cell(nLogs,1);
        for f=1:nLogs
            wNames{f} = newSWells(w).FracturesLogs(f).Name;
        end
        if (isempty(wNames))
            fprintf('**WARNNING: WELL::%s NO HAS FRACTURE LOGS\n',newSWells(w).Name)
            continue; 
        end
        
        % Open list dialog window to select logs
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [ilogs,ok] = listdlg('PromptString',...
            sprintf('Select from to well:%s the log to be renamed:',...
                newSWells(w).Name),...
            'SelectionMode','multiple',...
            'ListString',wNames);

        % user click Cancel or Close dialog box... so we leave here
        if (ok==0), continue; end
            
        
        % loop over logs selected
        for i=1:numel(ilogs)
            prompt={'New Fracture Log Name:'};
            dlg_title='Rename Fracture Log';
            numlines=1;
            def_ans = {newSWells(w).FracturesLogs(i).Name};
            answer=inputdlg(prompt,dlg_title,numlines,def_ans);

            % Cancel procedure... so we get out here
            if isempty(answer), continue; end

            newSWells(w).FracturesLogs(i).Name=answer{1};
        end    
    end
    
end


