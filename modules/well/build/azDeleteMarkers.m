function nW = azDeleteMarkers(W)
%Function to delete marker from well structure as described in well_structure.
%
% SYNOPSIS:
%   nW = azDeleteMarkers(W)
%
% PARAMETERS:
%  SWells - well structure used in analysis module to process some computation.
%           see well_structure.
%
% RETURNS:
%   W     - well structure modified after delete some marker.
%
% USAGE:
%   >> W = azDeleteMarkers(WOld);
%
% SEE ALSO:
%   menu, struct, well_structure, assert, listdlg, 
% 

    %% check input
    assert(isvector(W),'*ERROR: input variable must be a vector of structures');
    for i=1:numel(W)
        assert(isstruct(W(i)),...
            '*ERROR: all components of input must be a structure type')
    end
    
    
    %% choose marker(s) to delete
    
    % collecting markers
    mNames = []; m = 0;
    for i=1:numel(W)                            % loop over wells
        if (isempty(mNames))                    % special case first well
            for j=1:numel(W(i).Marker)          % add all markers!
                m = m + 1; mNames{m} = W(i).Marker(j).Name;
            end
        else
            for j=1:numel(W(i).Marker)          % loop over marker on i-th well
                hasMarker = false;              % check whether marker already added into mNames!
                for k=1:numel(mNames)
                    if (strcmpi(W(i).Marker(j).Name, mNames{k})) % Marker was already added into mNames
                        hasMarker = true; break;
                    end
                end
                if (~hasMarker)
                    m = m + 1; mNames{m} = W(i).Marker(j).Name; % adds markers into mNames
                end
            end            
        end
    end
 
    % List dialog window to select well 
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 
	
    [i,ok] = listdlg('PromptString','Select marker to remove from Project:',...
                    'SelectionMode','multiple',...
                    'ListString',mNames);

	% user click Cancel or Close dialog box... so we leave here
    if (ok==0)
        % copy
        nW = W;
        return; 
    end
    
    %% delete marker list data
    % If we get here, we will delete some marker
        
    % get marker to remove
    delMrk = mNames{i};
    % copy
	nW = W;
    % remove Marker field of nW
    nW = rmfield(nW,'Marker');
    for i=1:numel(nW)
        idx = true(numel(W(i).Marker),1);
        % check whether there is marker into current well
        for j=1:numel(W(i).Marker)          % loop over marker on i-th well
            if (strcmpi(W(i).Marker(j).Name, delMrk))
                idx(j) = false; break;      % Marker into i-th well must remove it
            end
        end
        nW(i).Marker = W(i).Marker(idx);
    end
end


