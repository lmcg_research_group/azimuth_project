function W = azRenameWell(SWells)
%Function to rename wells from well structure as described in well_structure.
%
% SYNOPSIS:
%   Wells = azRenameWell(SWells)
%
% PARAMETERS:
%  SWells - well structure used in the well analysis module to process some 
%         computation. see well_structure.
%
% RETURNS:
%   W     - well structure modified after renamed some wells.
%
% USAGE:
%   >> W= azRenameWell(WOld);
%
% SEE ALSO:
%   menu, struct, azvar_structure, listdlg, inputdlg
% 

    %% check input
    assert(isvector(SWells),'*ERROR: input variable must be a vector of structures');
    nWells = numel(SWells);
    for iwell=1:nWells
        assert(isstruct(SWells(iwell)),...
            '*ERROR: all components of input must be a structure type')
    end
    
    
    %% choose well(s) structure to delete
    
    % collecting name of wells
    wNames = cell(nWells,1);
    for iwell=1:nWells
        wNames{iwell} = SWells(iwell).Name;
    end
    % copy input to output
    W = SWells;
    
    % List dialog window to select well 
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 
	
    [iwell,ok] = listdlg('PromptString','Select well to rename:',...
                    'SelectionMode','multiple',...
                    'ListString',wNames);

	% user click Cancel or Close dialog box... so we leave here
    if (ok==0) return;  end
    
    %% rename well list data
    % If we get here, we will rename some well
    
    % loop over wells selected
    
    for iw=1:numel(iwell)
         prompt={'New Well Name:'};
        dlg_title='Rename Wells';
        numlines=1;
        def_ans = {W(iwell(iw)).Name};
        answer=inputdlg(prompt,dlg_title,numlines,def_ans);
        
        % Cancel procedure... so we get out here
        if isempty(answer), continue; end
        
        W(iwell(iw)).Name=answer{1};
    end
    
end


