function W = azDeleteWell(SWells)
%AZDELETEWELL Function to delete wells from well structure as described in well_structure.
%
% SYNOPSIS:
%   Wells = azDeleteWell(SWells)
%
% PARAMETERS:
%  SWells - well structure used in the well analysis module to process some 
%         computation. see azvar_structure.
%
% RETURNS:
%   W     - well structure modified after delete some wells.
%
% USAGE:
%   >> W= azDeleteWell(WOld);
%
% SEE ALSO:
%   menu, struct, azvar_structure, assert, listdlg, 
% 

    %% check input
    assert(isvector(SWells),'*ERROR: input variable must be a vector of structures');
    nWells = numel(SWells);
    for iwell=1:nWells
        assert(isstruct(SWells(iwell)),...
            '*ERROR: all components of input must be a structure type')
    end
    
    
    %% choose well(s) structure to delete
    
    % collecting name of wells
    wNames = cell(nWells,1);
    for iwell=1:nWells
        wNames{iwell} = SWells(iwell).Name;
    end
    
    % List dialog window to select well 
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 
	
    [iwell,ok] = listdlg('PromptString','Select well to remove from Project:',...
                    'SelectionMode','multiple',...
                    'ListString',wNames);

	% user click Cancel or Close dialog box... so we leave here
    if (ok==0)
        W = SWells;
        return; 
    end
    
    %% delete well list data
    % If we get here, we will delete some well
        
    % mark wells to remove
    idx=ones(1,nWells); idx(iwell)=0;
    iw=0;
    for i=1:nWells
        if (idx(i))
            iw=iw+1; 
            W(iw) = SWells(i); %#ok<*AGROW>
        end
    end
    
end


