function azXYPlots( W )
%Cross plots analysis.
%
% SYNOPSIS:
%   azXYPlots(W)
%
% PARAMETERS:
%   W   - structure used in the well analysis module to process some computation.
%         see azvar_structure.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azXYPlots(W)
%
% SEE ALSO:
%   menu, switch, azXYPlotConfig, azXYPlotSelect, 



%% check inputs
assert(isstruct(W),'**ERROR: Input must be a struct var type');
assert(isfield(W,'Name'),'**ERROR: struct var input must has ''Name'' field');
assert(isfield(W,'nWells'),'**ERROR: struct var input must has ''nWells'' field');
assert(isfield(W,'Wells'),'**ERROR: struct var input must has ''Wells'' field');


%% Analysis step

% oo loop to visit each procedures
azplot = 0; hfig=[];
while(azplot<3)
    azplot=menu('XY-Plots',...
        'Add Plot',...                  % azplot == 1
        'Select Plot',...               % azplot == 2
        'Exit/Return');                 % azplot == 3
    
    switch azplot
        case 1  % Add XY Plots
            h = azXYPlotConfig(W);
            if (isempty(hfig))
                hfig=h;
            else
                nh=numel(hfig);
                hfig(nh+1)=h; %#ok<AGROW>
            end
        case 2  % Select XY Plots
            if (~isempty(hfig)) % If user select Plot before add Plot do noting
                if (numel(hfig)>1)  % if has many figures
                    ih = azXYPlotSelect(hfig);
                    if (~isempty(ih))
                        h = azXYPlotConfig(W,hfig(ih));
                        hfig(ih)=h; %#ok<AGROW>
                    end
                else                  % if has one figure
                    h = azXYPlotConfig(W,hfig);
                    hfig=h;
                end
            end
    end
end


end

