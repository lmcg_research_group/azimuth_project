function figsNew = azXYPlotConfig( W, figs )
%Configure XY plots
%
% SYNOPSIS:
%   h=azXYPlotConfig(W,hfig)
%   h=azXYPlotConfig(W)
%
% PARAMETERS:
%   W    - structure used in the well analysis module to process some computation.
%          see azvar_structure.
%   figs - structure used to store handle figure objects and current figure id. 
%
% RETURNS:
%   h    - new/changed structure used to store handle figure objects and 
%          current figure id. 
%
% USAGE:
%   >> hfigNew=azXYPlotConfig(W,hfig)
%   >> h=azXYPlotConfig(W)
%
% SEE ALSO:
%   menu, Figure Properties, get, set, Figure handle, struct, listdlg, unique



%% check inputs
assert(isstruct(W),'**ERROR: Input must be a struct var type');
assert(isfield(W,'Name'),'**ERROR: struct var input must has ''Name'' field');
assert(isfield(W,'nWells'),'**ERROR: struct var input must has ''nWells'' field');
assert(isfield(W,'Wells'),'**ERROR: struct var input must has ''Wells'' field');

if nargin==2
    assert(isstruct(figs),'azFigures must be a struct var type')
    fplot = figs;
    wellcomeMsg = strcat('Edit figure-',num2str(fplot.CurrentFigure));
else
    fplot = [];
    wellcomeMsg='New Plot';
end



%% Configure Plot

% oo loop to visit each procedures
azplot = 0;
while(azplot<4)
    azplot=menu(wellcomeMsg,...
        'Set XY',...                 % azplot == 1
        'Del XY',...                 % azplot == 2
        'Plot',...                   % azplot == 3
        'Exit/Return');              % azplot == 4
    
    switch azplot
        case 1  % Set XY
            if (isempty(fplot))
                xydata = setData(W);
            else
                xydata = setData(W,fplot);
            end
            if (isempty(xydata))
                fprintf('*WARNING:: Datas setted is empty, set data again!\n')
            end
        case 2  % Del XY
            if (~isempty(fplot))
                fplot = delData(fplot);
            end
        case 3  % Plot
            if (isempty(fplot))
                fplot = azPlot(xydata);
            else
                fplot = azPlot(xydata,fplot);
            end
    end
end

figsNew = fplot;

end

%------------------------------------------------------------------------


%% Internal procedures

    function xy = setData(W,h)
        
        %%% Check inputs
        Attrib ={'DipAzimuth','Dip','Strike'};
        var = zeros(2,1);
        if nargin==2
            switch lower(h.XLabel)
                case 'dipazimuth'
                    var(1)=1; % DipAz
                case 'dip'
                    var(1)=2; % Dip
                case 'strike'
                    var(1)=3; % Strike
            end
            switch lower(h.YLabel)
                case 'dipazimuth'
                    var(2)=1; % DipAz
                case 'dip'
                    var(2)=2; % Dip
                case 'strike'
                    var(2)=3; % Strike
            end
        end
        
        %-------------------------
        %%% choose well structure
        % collecting name of wells
        wNames = cell(W.nWells,1);
        for iwell=1:W.nWells
            wNames{iwell} = W.Wells(iwell).Name;
        end

        % Open list dialog window to select well 
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [iwell,ok] = listdlg('PromptString','Select well to set Data plot',...
                        'SelectionMode','single',...
                        'ListString',wNames);

        % user click Cancel or Close dialog box... so we leave here
        if (ok==0)
            xy = [];
            return;
        end
        
        %--------------------------------
        %%% choose fracture logs to open
        % collecting name of fracture logs
        FNames = cell(numel(W.Wells(iwell).FracturesLogs),1);
        for i=1:numel(W.Wells(iwell).FracturesLogs)
            FNames{i} = W.Wells(iwell).FracturesLogs(i).Name;
        end

        % Open list dialog window to select well 
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [ifra,ok] = listdlg('PromptString','Select Log',...
                        'SelectionMode','single',...
                        'ListString',FNames);
        % user click Cancel or Close dialog box... so we leave here
        if (ok==0)
            xy = [];
            return;
        end
        
        
        %--------------------------------
        %%% choose Category
        % collecting name of categories fracture logs
        
        Category = unique(cellstr(W.Wells(iwell).FracturesLogs(ifra).Type));
        
        % Open list dialog window to select well 
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [typ,ok] = listdlg('PromptString','Select Category',...
                        'SelectionMode','single',...
                        'ListString',Category);
                    
        % user click Cancel or Close dialog box... so we leave here
        if (ok==0)
            xy = [];
            return;
        end
        
        ind = strcmp(cellstr(W.Wells(iwell).FracturesLogs(ifra).Type),...
                     Category(typ))~=0;

        id=['x','y'];
        for i=1:2
                        
            if (nargin==1)
                % Open list dialog window to select well 
                ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                             %            ok=0 if user click the Cancel button or close the dialog box 

                [var(i),ok] = listdlg('PromptString',strcat(upper(id(i)),...
                                                         '-Data to plot'),...
                                   'SelectionMode','single',...
                                   'ListString',Attrib);
                % user click Cancel or Close dialog box... so we leave here
                if (ok==0)
                    xy = [];
                    return;
                end
            end
                
            switch var(i)
                case 1  % DipAzimuth [0,360]
                    values = W.Wells(iwell).FracturesLogs(ifra).Az(ind);
                case 2  % Dip [0,90]
                    values = W.Wells(iwell).FracturesLogs(ifra).Dip(ind);
                case 3  % Striker [0,180]
                    values = (W.Wells(iwell).FracturesLogs(ifra).Az(ind)+90);
                    idx=find(values>360); 
                    if (~isempty(idx)), values(idx)=values(idx)-360; end
                    idx=find(values>180); 
                    if (~isempty(idx)), values(idx)=values(idx)-180; end
                    
            end

            xy(i) = struct('leg',strcat(W.Wells(iwell).Name,'/',...
                                 W.Wells(iwell).FracturesLogs(ifra).Name,...
                                 '/',Category(typ)),...
                           'label',Attrib{var(i)},...
                           'val',values); %#ok<AGROW>
        end
    end


%------------------------------------------------------------------------
    
    function hh = azPlot(xy,hfig)
    
    col = ['r';'b';'g';'c';'m'];
    xx=xy(1).val; yy=xy(2).val; 
    xxLabel=xy(1).label; yyLabel=xy(2).label;
    if nargin<2        
        figure;
        fh=plot(xx,yy,'LineStyle','none','Marker',get_marker(1),...
            'Color',get_rgb(col(1)),'DisplayName',xy(1).leg);
        xlabel(xxLabel); ylabel(yyLabel);
        legend('show');
    else
        nplt=numel(hfig.figure_handle);
       
        i1 = mod(nplt+1,length(col));
        if (i1==0), i1=length(col); end;
        i2 = mod(nplt+1,13);
        if (i2==0), i2=13; end;
    
        set(0,'CurrentFigure',hfig.CurrentFigure)
        hold on
        fh(nplt+1)=plot(xx,yy,'LineStyle','none','Marker',get_marker(i2),...
                        'Color',get_rgb(col(i1)),'DisplayName',xy(1).leg);
        fh(1:nplt)=hfig.figure_handle;
        legend('off');
        legend('show');
        hold off
    end

    hh=struct('figure_handle',fh,...
              'XLabel',xxLabel,...
              'YLabel',yyLabel,...
              'CurrentFigure',gcf);
          
    end

    
    %----------------------------------------------------------------------

    function he = delData(h)
        
        % get Number of plot on figure        
        nplt=numel(h.figure_handle);
        
        % Collect Names of plots
        pName = cell(nplt,1);
        for iplt=1:nplt
            pName{iplt} = get(h.figure_handle(iplt),'DisplayName');
        end
        
        % Open list dialog window to select well 
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [iplt,ok] = listdlg('PromptString','Select Data to delete',...
                        'SelectionMode','multiple',...
                        'ListString',pName);

        % user click Cancel or Close dialog box... so we leave here
        if (ok==0)
            he = h;
            return;
        end
        
        % set Current Figure
        set(0,'CurrentFigure',h.CurrentFigure);
        % Make invisible data selected
        set(h.figure_handle(iplt),'HandleVisibility','off','Visible','off');
        % reset legend
        legend('off');
        % make new legend
        legend('show');
        % removing deleted data
        idx = true(nplt,1); idx(iplt) = false;
        he=h;
        he.figure_handle = h.figure_handle(idx);
    end


    %----------------------------------------------------------------------



