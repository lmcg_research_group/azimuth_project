function ifg = azXYPlotSelect( figs )
%Select XY plots
%
% SYNOPSIS:
%   i=azXYPlotsSelect(figures)
%
% PARAMETERS:
%   figures - structure used to store handle figure objects and current 
%             figure id.
%
% RETURNS:
%   i       - index into figures to add/delete some data.
%
% USAGE:
%   >> ih=azXYPlotsSelect(hs)
%
% SEE ALSO:
%   menu, Figure Properties, Figure handle, struct, get, set, listdlg



    %% check inputs
    assert(isstruct(figs),'**ERROR: Input must be a struct var type');
    assert(isfield(figs,'figure_handle'),...
        '**ERROR: struct var input must has ''figure_handle'' field');
    assert(isfield(figs,'CurrentFigure'),...
        '**ERROR: struct var input must has ''CurrentFigure'' field');


    %% Select Plot

    %%% choose figure to change/add plot
    % collecting name of Figures opened
    nfigs = numel(figs);

    FNames = cell(nfigs,1);
    for i=1:nfigs
        FNames{i} = strcat('figure-',num2str(figs(i).CurrentFigure));
    end

    % Open list dialog window to select figures
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 

    [ifg,ok] = listdlg('PromptString','Select figure plot',...
                       'SelectionMode','single',...
                       'ListString',FNames);

    % user click Cancel or Close dialog box... so we leave here
    if (ok==0)
        ifg = [];
        return;
    end
        
          
end








