function azAnalysis(W)
%Function to drive well data analysis.
%
% SYNOPSIS:
%   azAnalysis(W)
%
% PARAMETERS:
%   W   - structure used in the well analysis module to process some computation.
%         see azvar_structure.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azAnalysis(W)
%
% SEE ALSO:
%   menu, assert, struct, azXYPlots, azLogsAnalysis, azRoseAnalysis, azStereogramAnalysis
% 
%

%% check inputs
assert(isstruct(W),'**ERROR: Input must be a struct var type');
assert(isfield(W,'Name'),'**ERROR: struct var input must has ''Name'' field');
assert(isfield(W,'nWells'),'**ERROR: struct var input must has ''nWells'' field');
assert(isfield(W,'Wells'),'**ERROR: struct var input must has ''Wells'' field');


wellcomeMsg = 'Well Analysis Module';
azwell=0;
is2Exit = 4;


%% Drive step

% oo loop to visit each procedures
while(azwell< is2Exit)
    azwell=menu(wellcomeMsg,...
        'XY-Plots',...              % azwell == 1
        'Rose Plot',...             % azwell == 2
        'Well Log Viewer',...       % azwell == 3
        'Exit/Return');             % azwell == 4
    
    switch azwell
        case 1  % X-Y Plots
            azXYPlots(W)
        case 2  % X-Y Plots
            azRoseAnalysis(W)
        case 3  % X-Y Plots
            azLogAnalysis(W)
    end

end

end

