function azLogAnalysis( W )
%Well Log Viewer plots analysis.
%
% SYNOPSIS:
%   azLogAnalysis(W)
%
% PARAMETERS:
%   W   - structure used in the well analysis module to process some computation.
%         see azvar_structure.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azLogAnalysis(W)
%
% SEE ALSO:
%   menu, switch



%% check inputs
assert(isstruct(W),'**ERROR: Input must be a struct var type');
assert(isfield(W,'Name'),'**ERROR: struct var input must has ''Name'' field');
assert(isfield(W,'nWells'),'**ERROR: struct var input must has ''nWells'' field');
assert(isfield(W,'Wells'),'**ERROR: struct var input must has ''Wells'' field');


%% Analysis step

% oo loop to visit each procedures
azlog = 0; hfig=[];
is2Exit = 3;

while(azlog<is2Exit)
    azlog=menu('Well Log Plots',...
        'Add Plot',...                  % azlog == 1
        'Select Plot',...               % azlog == 2
        'Exit/Return');                 % azlog == 3
    
    switch azlog
        case 1  % Add well log Plots
            h = azLogConfig(W);
            if (isempty(hfig))
                hfig=h;
            else
                nh=numel(hfig);
                hfig(nh+1)=h; %#ok<AGROW>
            end
        case 2  % Select Log Plots
            if (~isempty(hfig)) % If user select Plot before add Plot do noting
                if (numel(hfig)>1)  % if has many figures
                    ih = azLogSelect(hfig);
                    if (~isempty(ih))
                        h = azLogConfig(W,hfig(ih));
                        hfig(ih)=h; %#ok<AGROW>
                    end
                else                  % if has one figure
                    h = azLogConfig(W,hfig);
                    hfig=h;
                end
            end
    end
end


end

