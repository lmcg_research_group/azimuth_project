function ff=azTadpole(Data, tags, varargin)
% Display tadpole plot.
% 
% SYNOPSIS:
%   fh = azTadpole(data, tags, 'pn', pv) 
%
% PARAMETERS:
%   data    - struct container. this struct variable storage azimuth
%             (data.Az), Dip (data.Dip) and measured depth (data.MD).
%   tags    - cell string vector containing names/tags of categories that 
%             corresponds to data.az(i) observation. numel(name)=numel(data
%             .az)
%  'pn'/pv  - List of property names/property values.  OPTIONAL.
% 
%   PROPERTIY NAME  :: DESCRIPTION
%   FigureHandle    :: Figure handle to draw tadpole plot
%
% RETURNS:
%   Fh      - new/changed structure used to store handle figure objects and 
%             current figure id. 
%
% USAGE:
%   >> figure_hangle = azTadpole(DipAz, categories) 
%   >> figure_hangle = azTadpole(data, names, 'FigureHandle', h)
%   >> azTadpole(data, names, 'FigureHandle', h)
%
% SEE ALSO:
%   menu, Figure Properties, get, set, Figure Handle, patch, hist, unique

%% Check function call
if nargin<2
    error('azTadpole needs at least two inputs');
elseif mod(length(varargin),2)~=0
    error('Inputs must be paired: azTadpole(data,categorie,''PropertyName'',PropertyValue,...)');
elseif ~isstruct(Data)
    error('Data variable must be a structure var type.');
elseif ~iscell(tags)
    error('tags variable must be a cell var type.');
end


%% Default parameters
ff           =[];


%% User settings
for i=1:2:numel(varargin)
    switch lower(varargin{i})
        case 'figurehandle'
            ff     = varargin{i+1};
        otherwise
            error([varargin{i} ' is not a valid property for rose3 function.']);
    end
end


%% Process input data
az  = Data.Az-90; 
dip = Data.Dip;
md  = Data.MD;
Category     = unique(cellstr(tags));

% define color to display
color = feval('jet',256);
try
    color = interp1(linspace(1,length(Category),256),color,1:length(Category));
catch %#ok<CTCH>
    color = interp1(linspace(1,length(Category)+1,256),color,1:length(Category)+1);
end


%% Compute data
% Transform spherical coordinates to Cartesian
[u, v]=pol2cart(rad(az),1);

% Create figure
if isempty(ff)
    figure1 = figure('Color',[1 1 1]);
else
    set(0,'CurrentFigure',ff)
    clf;
    figure1 = ff;
end

% Create axes
axes1 = axes('Parent',figure1,'YDir','reverse',...
    'XTick',[0 10 20 30 40 50 60 70 80 90],...
    'XAxisLocation','top');

set(get(gca,'XLabel'),'String','Dip (degree)')
set(get(gca,'YLabel'),'String','MD')

grid(axes1,'on');
hold(axes1,'all');

% Create tadpole
nplt = length(Category);
for i=1:nplt
    % index of current category
    idx = strcmp(cellstr(tags),Category{i})~=0;
    
    j = mod(nplt+1,13);
    if (j==0), j=13; end;
    
    plot(dip(idx),md(idx),'MarkerSize',12,'MarkerEdgeColor',color(i,:),...
        'MarkerFaceColor',color(i,:),'Marker','.',...
        'LineStyle','none','DisplayName', strrep(Category{i}, '_', ' '))
    
    quiver(dip(idx),md(idx),u(idx),v(idx),'ShowArrowHead','off',...
        'MarkerSize',12,'MarkerEdgeColor',color(i,:),...
        'MarkerFaceColor',color(i,:),'Marker','.',...
        'Color',color(i,:),'AutoScaleFactor',0.27,...
        'HandleVisibility','off');
end
% Create title
% title('Tadpole');


xlim([0,90])
% ylim('auto')

% Create legend
legend('show','Location','NorthWestOutside')


ff=figure1;

end

