function figsNew = azRoseConfig( W, figs )
% 
%Configure Rose plots
%
% SYNOPSIS:
%   h=azRoseConfig(W,hfig)
%   h=azRoseConfig(W)
%
% PARAMETERS:
%   W    - structure used in the well analysis module to process some computation.
%          see azvar_structure.
%   figs - structure used to store handle figure objects and current figure id. 
%
% RETURNS:
%   h    - new/changed structure used to store handle figure objects and 
%          current figure id. 
%
% USAGE:
%   >> hfigNew=azRoseConfig(W,hfig)
%   >> h=azRoseConfig(W)
%
% SEE ALSO:
%   menu, Figure Properties, get, set, Figure handle, struct, listdlg, unique



%% check inputs
assert(isstruct(W),'**ERROR: Input must be a struct var type');
assert(isfield(W,'Name'),'**ERROR: struct var input must has ''Name'' field');
assert(isfield(W,'nWells'),'**ERROR: struct var input must has ''nWells'' field');
assert(isfield(W,'Wells'),'**ERROR: struct var input must has ''Wells'' field');

if nargin==2
    assert(isstruct(figs),'azFigures must be a struct var type')
    fplot = figs;
    wellcomeMsg = strcat('Edit figure-',num2str(fplot.CurrentFigure));
else
    fplot = [];
    wellcomeMsg='New Plot';
end



%% Configure Plot

% oo loop to visit each procedures
azplot  = 0;
is2Exit = 3;

while(azplot<is2Exit)
    azplot=menu(wellcomeMsg,...
        'Set Data',...              % azplot == 1
        'Plot',...                  % azplot == 2
        'Exit/Return');             % azplot == 3
    
    switch azplot
        case 1  % Set Data
            if (isempty(fplot))
                [data,names] = setData(W);
            else
                [data, names] = setData(W,fplot);
            end
            if (isempty(data))
                fprintf('*WARNING:: Datas setted is empty, set data again!\n')
            end
        case 2  % Plot
            if (isempty(fplot))
                fplot = azPlot(data,names);
            else
                fplot = azPlot(data,names,fplot);
            end
    end
end

figsNew = fplot;

end

%------------------------------------------------------------------------


%% Internal procedures

    function [data, entities] = setData(W,h)
        %--------------------------------
        % Initialise outs
        data = []; entities = [];
        
        
        %--------------------------------
        % To select data that must remain
        if (nargin==2)
            DataName = cellstr(h.Catagories);
            Categories = unique(DataName);
            % Open list dialog window to select reamind data 
            ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                         %            ok=0 if user click the Cancel button or close the dialog box                         
            [idat,ok] = listdlg('PromptString','Select data that must remain',...
                            'SelectionMode','multiple',...
                            'ListString',Categories);

            % user click Cancel or Close dialog box... so we leave here            
            if (ok==0), return; end

            remain = Categories(idat);
            
            % collecting data
            idx = false(1,length(DataName));

            for i=1:numel(idat)
                ind = strcmp(DataName,remain(i))~=0;
                idx(ind)=true;
            end
            DataName=DataName(idx);
            DataValue = h.Data.Values(idx);
        end
        
        
        %--------------------------------
        % choose well structure
        % collecting name of wells
        wNames = cell(W.nWells,1);
        for iwell=1:W.nWells
            wNames{iwell} = W.Wells(iwell).Name;
        end

        % Open list dialog window to select well 
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [iwell,ok] = listdlg('PromptString','Select well to set Data plot',...
                        'SelectionMode','single',...
                        'ListString',wNames);

        % user click Cancel or Close dialog box... so we leave here
        if (ok==0), return; end
        
        
        %--------------------------------
        %%% choose fracture logs to open
        % collecting name of fracture logs
        FNames = cell(numel(W.Wells(iwell).FracturesLogs),1);
        for i=1:numel(W.Wells(iwell).FracturesLogs)
            FNames{i} = W.Wells(iwell).FracturesLogs(i).Name;
        end

        % Open list dialog window to select well 
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [ifra,ok] = listdlg('PromptString','Select Log',...
                        'SelectionMode','single',...
                        'ListString',FNames);
        % user click Cancel or Close dialog box... so we leave here
        if (ok==0), return; end
        
        
        %--------------------------------
        %%% choose Category
        % collecting name of categories fracture logs
        
        Category = unique(cellstr(W.Wells(iwell).FracturesLogs(ifra).Type));
        
        % Open list dialog window to select well 
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [typ,ok] = listdlg('PromptString','Select Category',...
                        'SelectionMode','multiple',...
                        'ListString',Category);
                    
        % user click Cancel or Close dialog box... so we leave here
        if (ok==0), return; end
        
        % collect all indexes
        idx = false(1,length(W.Wells(iwell).FracturesLogs(ifra).Type));
        
        for i=1:numel(typ)
            ind = strcmp(cellstr(W.Wells(iwell).FracturesLogs(ifra).Type),...
                     Category(typ(i)))~=0;
            idx(ind)=true;
        end     
        names=cellstr(W.Wells(iwell).FracturesLogs(ifra).Type);
        
        
        %--------------------------------
        % Select type of rose to display -> [0,180](applied for bedding) 
        %                                   or [0,360] (applied for fractures)
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [typ,ok] = listdlg('PromptString','Rose type',...
                        'SelectionMode','single',...
                        'ListString',{'DipAz [0,360]','Strike [0,180]'});
                    
        % user click Cancel or Close dialog box... so we leave here
        if (ok==0), return; end 
                
        
        %--------------------------------
        % Select multiplot or single plot
        pltyp =[];
        if (nargin==1)
            ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                         %            ok=0 if user click the Cancel button or close the dialog box 

            [pltyp,ok] = listdlg('PromptString','Plot Mode',...
                            'SelectionMode','single',...
                            'ListString',{'single','multiple'});

            % user click Cancel or Close dialog box... so we leave here
            if (ok==0), return; end    
        end

        
        %--------------------------------
        % set data
        
        data = struct('Values',W.Wells(iwell).FracturesLogs(ifra).Az(idx),...
                      'RoseType',typ,'PlotMode',pltyp);
        entities=strcat(W.Wells(iwell).Name,'/',...
                        W.Wells(iwell).FracturesLogs(ifra).Name,'/',...
                        names(idx));
                    
        if (nargin==2)
            entities = [entities; DataName];
            data.Values = [data.Values; DataValue];
        end
        
    end


    %----------------------------------------------------------------------
    
    function hh = azPlot(data,names,hfig)
    
    val  = data.Values; type_ = data.RoseType; pmode = data.PlotMode;
    
    if nargin<3 
        %
        fh = azRose3(val,names,'Type',type_,'PlotMode',pmode);

    else
        % rose plot
        pmode = 1;
        fh = azRose3(val,names,'Type',type_,'PlotMode',pmode,...
                     'FigureHandle',hfig.CurrentFigure);
    end

    if (pmode==1)
        hh=struct('figure_handle',fh,...
                  'Data',data,...
                  'Catagories',char(names),...
                  'CurrentFigure',gcf);
    else
        nplt = length(fh);
        Category = unique(names);
        for i=1:nplt
            idx = strcmp(names,Category(i))~=0;
            rdata = struct('Values',val(idx),...
                      'RoseType',type_,'PlotMode',pmode);
            set(0,'CurrentFigure',fh(i))
            hh(i)=struct('figure_handle',fh(i),...
                      'Data',rdata,...
                      'Catagories',char(names(idx)),...
                      'CurrentFigure',gcf);
        end
    end
          
    end

    
    


