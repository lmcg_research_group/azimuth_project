% ROSE
%
% Files
%   azRose3        - This is a replacement of MATLAB's rose for those in the geophysical 
%   azRoseAnalysis - Rose plots analysis.
%   azRoseConfig   - Configure Rose plots
%   azRoseSelect   - Select Rose plots
