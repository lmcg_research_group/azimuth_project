function ff = azRose3(az, name, varargin) 
% This is a replacement of MATLAB's rose for those in the geophysical 
% sciences.
% 
% SYNOPSIS:
%   fh = azRose3(az, name, 'pn', pv) 
%
% PARAMETERS:
%   az      - values of dip azimuths (in degrees) vector 
%   name    - cell string vector containing name of categories that corresponds
%             to az(i) observation. numel(name)=numel(az)
%  'pn'/pv  - List of property names/property values.  OPTIONAL.
% 
%   PROPERTIY NAME  :: DESCRIPTION
%   nb              :: Number of bins (subdivisions) to be shown (DEFAULT, 36).
%   Type            :: =1 (DEFAULT) for 360 degree data (dipAzimuth), 
%                      =2 for 180 degree data (strike).
%   FScale          :: 0 for arithmetic (DEFAULT), 1 for square root scale for frequency.
%   NFrequencies    :: Draw this number of circles indicating frequency (DEFAULT, 4).
%   TextColor       :: Text and axis color, any admitted matlab color format. 
%                      Default is black.
%   Category        :: List of categories to be plotted on rose.
%   PlotMode        :: =1 all data are plotted in one plot (DEFAULT);
%                      =2 data are plotted in multiples plots
%   FigureHandle    :: Figure handle to draw rose plot
%
% RETURNS:
%   Fh      - new/changed structure used to store handle figure objects and 
%             current figure id. 
%
% USAGE:
%   >> figure_hangle = azRose3(DipAz, categories, 'nb', 72, 'Type', 2) 
%   >> figure_hangle = azRose3(az, names, 'NFrequencies', 12, 'PlotMode', 2) 
%
% SEE ALSO:
%   menu, Figure Properties, get, set, Figure Handle, patch, hist, unique

% fprintf(1,' * into rose3\n');  % for dbg

%% Check funciton call
if nargin<2
    error('rose3 needs at least two inputs');
elseif mod(length(varargin),2)~=0
    error('Inputs must be paired: rose3(az,''PropertyName'',PropertyValue,...)');
elseif ~isnumeric(az)
    error('az variable must be numeric arrays.');
end



%% Default parameters
nb           = 36;
typ          = 1;
fscale       = 0;
NFrequencies = 4;
TextColor    = 'k';
Category     = unique(cellstr(name));
PlotMode     = 1;
ff           =[];

%% User settings
for i=1:2:numel(varargin)
    switch lower(varargin{i})
        case 'nb'
            nb           = varargin{i+1};
        case 'type'
            typ          = varargin{i+1};
        case 'fscale'
            fscale       = varargin{i+1};
        case 'nfrequencies'
            NFrequencies = varargin{i+1};
        case 'textcolor'
            TextColor    = varargin{i+1};
        case 'category'
            Category     = varargin{i+1};
        case 'plotmode'
            PlotMode     = varargin{i+1};
        case 'figurehandle'
            ff     = varargin{i+1};
        otherwise
            error([varargin{i} ' is not a valid property for rose3 function.']);
    end
end


%% process data

if typ == 2    % double, then divide by 2 if necessary
  k = find(az > 179.99);
  az(k) = az(k) - 180;    % corrected az
  az2 = [az;(az+180)];    % extended az
end
x = [0:360/nb:360]; %#ok<*NBRAK>
x2 = [180/nb:360/nb:(360 - 180/nb)];  % class limits

%% compute frequency

% figure(); % for dbg
if typ == 1        
  [f,x2] = hist(az,x2);     %#ok<*NASGU> % draw histogram
%   stairs(x, [f 0]);       % plot histogram  % for dbg
else
  xxx = [0:360/nb:180];    % change x scale
  x3 = [180/nb:360/nb:180-180/nb];   % frequencies for histogram
  [f,x3] = hist(az,x3);   %#ok<*ASGLU> % draw histogram
%   stairs(xxx, [f 0]);  % for dbg
  [f, x2] = hist(az2, x2); % frequencies for rose 
end

ftot = sum(f);    % find total frequency
if typ == 2
  ftot = ftot/2;  % modify if 180 data
end

%% parse data

% define color to display
color = feval('jet',256);
try
    color = interp1(linspace(1,length(Category),256),color,1:length(Category));
catch %#ok<CTCH>
    color = interp1(linspace(1,length(Category)+1,256),color,1:length(Category)+1);
end

fmax = 0; % frequency max
fall = zeros(length(Category),nb); % storage frequencies of all categories
for cat=1:length(Category)
    % index of such category(cat)
    idx = strcmp(cellstr(name),Category{cat})~=0;
    azz=az(idx); % get dipAz of current category
    if typ == 2    % double, then divide by 2 if necessary
      k = find(azz > 179.99);
      azz(k) = azz(k) - 180;    % corrected az
      az2 = [azz;(azz+180)];    % extended az
    end
    
    x2 = [180/nb:360/nb:(360 - 180/nb)];  % class limits
    if typ == 1        
      [f,x2] = hist(az(idx),x2);   %#ok<*NASGU> % draw histogram
    else
      xxx = [0:360/nb:180];    % change x scale
      x3 = [180/nb:360/nb:180-180/nb];   % frequencies for histogram
      [f,x3] = hist(az(idx),x3);   %#ok<ASGLU> % draw histogram
      [f, x2] = hist(az2, x2); % frequencies for rose 
    end
    fmax = max([fmax,max(f)]);    % set up axes for histogram
    fall(cat,:) = f;              % set frequency of current category
    if (~isempty(regexpi(Category{cat},'bedding')))
        color(cat,:) = [0, 1, 0];  % if category is bedding set color green
    end
end


%% begin new figure for rose
if (PlotMode==1)

    if fscale == 1   % scale frequency by maximum
       f = sqrt(f); 
       fmax = max(f);
       f0 = fmax/10;
    else
       f0 = fmax/10;
    end
    if (isempty(ff)) 
        figure('Color','w'); % new figure outs
    else
        set(0,'CurrentFigure',ff)
        clf;
    end
    cax=DrawGrids(fmax,NFrequencies,ftot, TextColor); % draw grid of rose
    SingleRose(Category, fall, x, color, cax)         % draw data
    hold off
    legend('show','Location','Best')
    ff = gcf; % get current figure handle [outs]
else
    for i=1:length(Category)
        if fscale == 1   % scale frequency by maximum
           f = sqrt(fall(i,:));
           fmax = max(fall(i,:));
           f0 = fmax/10;
        else
           fmax = max(fall(i,:));
           f0 = fmax/10;
        end
        
        ftot = sum(fall(i,:));    % find total frequency
        if typ == 2
          ftot = ftot/2;  % modify if 180 data
        end
        figure('Color','w'); % new figure outs
        cax=DrawGrids(fmax,NFrequencies,ftot, TextColor); % draw grid of rose
        MultipleRose(Category{i}, fall(i,:), x, color(i,:), cax) % draw data
        title(Category{i}) % set plot title
        ff = [ff; gcf]; %#ok<*AGROW> % collect current figures handle [outs]
    end
end

end

%% private functions

function SingleRose(Category, fall, x, color, cax)
% This is to plot all data in one plot (figure).
% 
nb = size(fall,2);
dolegend = true(length(Category),1); jj = 0; 
for i = 1:nb     % draw arcs
    if (any(fall(:,i)))
        buff=zeros(length(Category),2);
        buff(:,1)=fall(:,i); buff(:,2)=1:length(Category);
        buff = sortrows(buff);

        for j=length(Category):-1:1
            f = buff(j,1);
            azinc = 5;
            azc = linspace(x(i),x(i+1),azinc+1);
            len = length(azc);
            rs = zeros(len,1);
            rc = zeros(len,1);
            rs = f*sin(pi*azc/180);
            rc = f*cos(pi*azc/180);
    %         plot(rs, rc,'y','LineWidth',2)
    %         fprintf(1,'%f %f %f\n',f(i), x(i), x(i+1));

            rs10 = 0*sin(pi*x(i)/180);   % draw radial lines
            rc10 = 0*cos(pi*x(i)/180);
            rs1 = f .* sin(pi*x(i)/180);
            rc1 = f .* cos(pi*x(i)/180); 
    %         plot([rs10; rs1],[rc10; rc1],'y','linewidth',2);

            rs20 = 0*sin(pi*x(i+1)/180);
            rc20 = 0*cos(pi*x(i+1)/180);
            rs2 = f .* sin(pi*x(i+1)/180);
            rc2 = f .* cos(pi*x(i+1)/180);
    %         plot([rs20; rs2],[rc20; rc2],'r','linewidth',2);

            xx = [rs10, rs1, rs, rs20, rs2];
            yy = [rc10, rc1, rc, rc20, rc2];

            if (dolegend(buff(j,2)))
                patch('XData',xx, 'YData', yy, 'LineStyle', '-', ...
                      'FaceColor', color(buff(j,2),:), 'LineWidth', 1, ...
                      'FaceAlpha',1.0, 'HandleVisibility', 'on',...
                      'DisplayName',strrep(Category{buff(j,2)}, '_', ' '),...
                      'Parent', cax);
                dolegend(buff(j,2))=false;
            else
                jj=jj+1;
                patch('XData',xx, 'YData', yy, 'LineStyle', '-', ...
                      'FaceColor', color(buff(j,2),:), 'LineWidth', 1, ...
                      'FaceAlpha',0.75, 'HandleVisibility', 'off',...
                      'DisplayName',strrep(Category{buff(j,2)}, '_', num2str(jj)),...
                      'Parent', cax);
            end
        end
    end
end
end


function MultipleRose(Category, f, x, color, cax)
% This is to plot data in multiples plots (figure).
% 

nb = numel(f);
dolegend = true; jj=0;

for i = 1:nb     % draw arcs
    if (f(i))
        azinc = 5;
        azc = linspace(x(i),x(i+1),azinc+1);
        len = length(azc);
        rs = zeros(len,1);
        rc = zeros(len,1);
        rs = f(i)*sin(pi*azc/180);
        rc = f(i)*cos(pi*azc/180);
%         plot(rs, rc,'y','LineWidth',2)
%         fprintf(1,'%f %f %f\n',f(i), x(i), x(i+1));

        rs10 = 0*sin(pi*x(i)/180);   % draw radial lines
        rc10 = 0*cos(pi*x(i)/180);
        rs1 = f(i) .* sin(pi*x(i)/180);
        rc1 = f(i) .* cos(pi*x(i)/180); 
%         plot([rs10; rs1],[rc10; rc1],'y','linewidth',2);

        rs20 = 0*sin(pi*x(i+1)/180);
        rc20 = 0*cos(pi*x(i+1)/180);
        rs2 = f(i) .* sin(pi*x(i+1)/180);
        rc2 = f(i) .* cos(pi*x(i+1)/180);
%         plot([rs20; rs2],[rc20; rc2],'r','linewidth',2);

        xx = [rs10, rs1, rs, rs20, rs2];
        yy = [rc10, rc1, rc, rc20, rc2];

        if (dolegend)
            patch('XData',xx, 'YData', yy, 'LineStyle', '-', ...
                  'FaceColor', color, 'LineWidth', 1, ...
                  'FaceAlpha',1.0, 'HandleVisibility', 'on',...
                  'DisplayName',strrep(Category, '_', ' '),...
                  'Parent', cax);
            dolegend=false;
        else
            jj=jj+1;
            patch('XData',xx, 'YData', yy, 'LineStyle', '-', ...
                  'FaceColor', color, 'LineWidth', 1, ...
                  'FaceAlpha',0.75, 'HandleVisibility', 'off',...
                  'DisplayName',strrep(Category, '_', num2str(jj)),...
                  'Parent', cax);
        end
    end
end
end


function cax=DrawGrids(fmax,NFrequencies,ftot, TextColor) %#ok<*INUSL>
% Draw grid lines and ticks on rose graph
% 
    cax = newplot;

    next = lower(get(cax, 'NextPlot'));
    hold_state = ishold(cax); % get hold state

    % get x-axis text color so grid is in same color
    tc = get(cax, 'XColor');
    ls = get(cax, 'GridLineStyle');

    % Hold on to current Text defaults, reset them to the
    % Axes' font attributes so tick marks use them.
    fAngle = get(cax, 'DefaultTextFontAngle');
    fName = get(cax, 'DefaultTextFontName');
    fSize = get(cax, 'DefaultTextFontSize');
    fWeight = get(cax, 'DefaultTextFontWeight');
    fUnits = get(cax, 'DefaultTextUnits');
    set(cax, ...
        'DefaultTextFontAngle', get(cax, 'FontAngle'), ...
        'DefaultTextFontName', get(cax, 'FontName'), ...
        'DefaultTextFontSize', get(cax, 'FontSize'), ...
        'DefaultTextFontWeight', get(cax, 'FontWeight'), ...
        'DefaultTextUnits', 'data');

    % only do grids if hold is off
    if ~hold_state
        [xx,yy]     = cylinder(1,50); xx = xx(1,:); yy = yy(1,:);                        % Get x and y for a unit-radius circle

        radiusmax = fmax;
        patch('XData', xx*radiusmax, 'YData', yy*radiusmax, ...
            'EdgeColor', 'k', 'FaceColor', 'w', ...
            'HandleVisibility', 'off');                     % draw last circle
        hold on            
        cax=gca;
        set(cax, 'DataAspectRatio', [1, 1, 1]), axis(cax, 'off');

        str=strcat('N = ',num2str(ftot));
        ylim=get(gca,'YLim');
        xlim=get(gca,'XLim');
        text(xlim(2),ylim(1)+mean(xlim),str,...
                'VerticalAlignment','bottom',...
                'HorizontalAlignment','left') 
        CardinalLabels(radiusmax,TextColor);         % Display 0, 90, 180, 270

    end



end

   
function CardinalLabels(circlemax,TextColor)
% Draw poles (0, 90, 180, 270) on rose plot
% 

    text( circlemax,0,[' 90'],'HorizontalAlignment','left'  ,'verticalalignment','middle','color',TextColor); % East  label
    text(-circlemax,0,['270 '],'HorizontalAlignment','right' ,'verticalalignment','middle','color',TextColor); % West  label
    text(0, circlemax,'0'     ,'HorizontalAlignment','center','verticalalignment','bottom','color',TextColor); % North label
    text(0,-circlemax,'180'   ,'HorizontalAlignment','center','verticalalignment','top'   ,'color',TextColor); % South label
    xlim([-circlemax circlemax]);
    ylim([-circlemax circlemax]);
end 


