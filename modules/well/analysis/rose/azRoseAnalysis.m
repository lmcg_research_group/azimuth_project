function azRoseAnalysis( W )
%Rose plots analysis.
%
% SYNOPSIS:
%   azRoseAnalysis(W)
%
% PARAMETERS:
%   W   - structure used in the well analysis module to process some computation.
%         see azvar_structure.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azRoseAnalysis(W)
%
% SEE ALSO:
%   menu, switch



%% check inputs
assert(isstruct(W),'**ERROR: Input must be a struct var type');
assert(isfield(W,'Name'),'**ERROR: struct var input must has ''Name'' field');
assert(isfield(W,'nWells'),'**ERROR: struct var input must has ''nWells'' field');
assert(isfield(W,'Wells'),'**ERROR: struct var input must has ''Wells'' field');


%% Analysis step

% oo loop to visit each procedures
azrose = 0; hfig=[];
is2Exit = 3;

while(azrose<is2Exit)
    azrose=menu('Rose Plots',...
        'Add Plot',...                  % azrose == 1
        'Select Plot',...               % azrose == 2
        'Exit/Return');                 % azrose == 3
    
    switch azrose
        case 1  % Add Rose Plots
            h = azRoseConfig(W);
            if (isempty(hfig))
                hfig=h;
            else
                hfig=[hfig, h]; %#ok<AGROW>
            end
        case 2  % Select Rose Plots
            if (~isempty(hfig)) % If user select Plot before add Plot do noting
                if (numel(hfig)>1)  % if has many figures
                    ih = azRoseSelect(hfig);
                    if (~isempty(ih))
                        h = azRoseConfig(W,hfig(ih));
                        hfig(ih)=h; %#ok<AGROW>
                    end
                else                  % if has one figure
                    h = azRoseConfig(W,hfig);
                    hfig=h;
                end
            end
    end
end


end

