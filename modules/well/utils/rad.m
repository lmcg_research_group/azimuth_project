function r=rad(deg)
%RAD convert degrees to radians
r = pi*deg/180;
end
