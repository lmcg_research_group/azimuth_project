% Files
%   readDEV     - Read well data on PETREL format (*.dev files).
%   readMXY     - Read well data on MD DX DY format (*.mxy files).
%   readXYZ     - Read well data on XYZ format (*.xyz files).
%   Export2VTK  - Function to write a vtk file format to view well trajectory into ParaView.
%   azGetFile   - Native user interface to Open standard dialog box for retrieving files
%   get_rgb     - Wrap function to get rgb values related to string color provided.
%   filePreview - Print some line in files , it works like a preview file.
%   get_marker  - Wrap function to get marker (used um plot function) related to index provided.
