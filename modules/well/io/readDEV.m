function DEV = readDEV(fid)
%Read well data on PETREL format (*.dev files).
% 
% DEV structure:
% - KB -- 
% - MD --
% - X  --
% - Y  --
% - Z  --
% - TVD--
%
% SYNOPSIS:
%   DEV = readDEV(fid,wName,pathname);
%
% PARAMETERS:
%   fid      - integer file identifier.
%
% RETURNS:
%   DEV     - DEV structure used in this step to store read data.
%
% USAGE:
%   >> DEV = readDEV(fid,'W1B348','C:\B348\wells');
%
% SEE ALSO:
%   fgets, sscanf, struct, regexpi



% Read file until eof
i = 0;                                          % counter of number of data
while ~feof(fid)
    tline=fgets(fid);                           % get line
    % search for WELL KB on header
    if (~isempty(regexpi(tline,'# WELL KB')))   
        kelly = sscanf(tline,'# WELL KB: %f');
    end
    
    % get only numerical data on file
    [data,ndata] = sscanf(tline,'%f');
    if (ndata == 0), continue; end % there isn't numerical data on current line
    if (ndata>5) % there are more then 5 numerical data on current line
        i=1+i;   % increase counter of data read
        wmd(i)=data(1); wx(i)  =data(2); wy(i)=data(3); %#ok
        wz(i) =data(4); wtvd(i)=data(5);                %#ok
    elseif (ndata<5) % there are less then 5 numerical data on current line
        i=1+i;       % increase counter of data read
        wmd(i)=data(1); wx(i)  =data(2); wy(i)=data(3); %#ok
        wz(i) =data(4); wtvd(i)=0.;                     %#ok
    elseif (ndata<4) % there are less then 4 numerical data on current line
        i=1+i;       % increase counter of data read
        wmd(i)=data(1); wx(i)  =data(2); wy(i)=data(3); %#ok
        wz(i) =0.; wtvd(i)=0.;                          %#ok
    end
end

% const. DEV structure
DEV  = struct( 'KB', kelly, ...
               'X' ,    wx, ...
               'Y' ,    wy, ...
               'Z' ,    wz, ...
              'MD' ,   wmd, ...
              'TVD',  wtvd);

end

