function Export2VTK(Wells)
%Function to write a vtk file format to view well trajectory into ParaView.
%
% SYNOPSIS:
%   Export2VTK(azFiled)
%
% PARAMETERS:
%   W   - structure used in the well analysis module to process some computation.
%         see azvar_structure.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> Export2VTK(azFiled)
%
% SEE ALSO:
%   menu, listdlg, uigetdir, struct, azvar_structure
% 
%

if (isfield(Wells,'azField'))
    W=Wells.azField;
else
    W=Wells;
end

for w=1:W.nWells
    wNames{w}=W.Wells(w).Name;
end

v=0;
while (v==0)
    [s,v] = listdlg('PromptString','Select well(s):',...
                    'ListString',wNames);
end

folder_name = uigetdir('.','Select Directory to save vtk file(s)...');

for i=1:length(s)
    writeVTK(W.Wells(s(i)),folder_name);
end
end


function  writeVTK(Data, path_)
% Write vtk format
% 
    if (nargin>1)
        lpath=path_;
    else
        lpath='.';
    end

    fvtk = fopen(strcat(lpath,filesep,Data.Name,'_well.vtk'),'w');

    fprintf(fvtk,'# vtk DataFile Version 2.1\n %s - polyline\n ASCII\n\n',...
        Data.Name);
    
    np = length(Data.MD);
    fprintf(fvtk,'DATASET POLYDATA\nPOINTS %u float\n', np);
    for i=1:np
        fprintf(fvtk,'%f %f %f\n ', Data.X(i), Data.Y(i), -Data.Z(i));
    end
    fprintf(fvtk,'LINES 1 %u\n', np+1);
    j=0;
    fprintf(fvtk,'%u ', np);
    for i=1:np
        fprintf(fvtk,'%u ', i-1);
        j=j+1;
        if (mod(j,20)==0)
            fprintf(fvtk,'\n');
        end
    end

end