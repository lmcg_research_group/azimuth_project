function filePreview(filename,fid,bline,eline)
%Print some line in files , it works like a preview file.
fprintf('\n\nPreview file: %s\n',filename)
for i=1:eline
    if (i>=bline)
        fprintf('%u %s',i,fgets(fid))
    end
end
end

