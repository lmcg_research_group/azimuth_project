function [filename,pathname,fid,status]=azGetFile(ext,title_,dir_,multi)
%Native user interface to Open standard dialog box for retrieving files
% 
% SYNOPSIS
%	[filename,pathname,fid,status]=azGetFile(ext,title_,dir_,multi)
%	[filename,pathname,fid,status]=azGetFile(ext,title_,dir_)
%	[filename,pathname,fid,status]=azGetFile(ext,title_)
%	[filename,pathname,fid,status]=azGetFile(ext)
%
% PARAMETERS
%	ext       - char type that express extension of files that will 
%               displayed only those files with extensions that match!.
%	title_    - string to display on windows title that will be opened 
%               during pick process of file.
%   dir_      - argument to specify a start path and a default file name 
%               for the dialog box
%   multi     - argument to (dis)enables multiple-file selection (default 
%               is disable {'off'}).
% 
% RETURNS
%	filename  - File name of file selected.
%	pathname  - Path of file selected.
%   fid       - OPTIONAL, An integer that identifies the file for all 
%               subsequent low-level file I/O operations
%   status    - OPTIONAL, the index of the filter selected in the dialog box. 
%               If you click Cancel or close the dialog window, the function
%               sets FilterIndex to 1.
% 
% USAGE
%	[filename, pathname] = azGetFile_('*.dat');
%   [Fname,Fpath,fid]=azGetFile('*.F90','Select the FORTRAN source code','C:\','on')
% 
% SEE ALSO
%   inputdlg, uigetfile, error
% 

switch nargin
    case 1
        ltitle=sprintf('Select file %s',ext); ldir=cd; lmulti='off';
    case 2
        if (~isempty(title_))
            ltitle=title_;
        else
            ltitle=sprintf('Select file %s',ext);
        end
        ldir=cd; lmulti='off';
    case 3
        if (isempty(title_))
            ltitle=sprintf('Select file %s',ext); ldir=dir_; %#ok
        else
            ltitle=title_;  ldir=dir_; %#ok
        end
        if (isempty(dir_))
            ldir=cd;
        else
            ldir=dir_;
        end
         lmulti='off';
    case 4
        if (isempty(title_))
            ltitle=sprintf('Select file %s',ext); ldir=dir_; %#ok
        else
            ltitle=title_;  ldir=dir_; %#ok
        end
        if (isempty(dir_))
            ldir=cd;
        else
            ldir=dir_;
        end
         lmulti=multi;
    otherwise
        error('***Error:azGetFile must be one input (ext) at least!!!')
end

% In this way we force (brutally!!) the uigetfile works
filename = 0;
while (isnumeric(filename))
    [filename, pathname,status] = uigetfile(ext,ltitle,ldir,'MultiSelect',lmulti); % choose file to open
end

status = abs(status-1); 
if (status==1)
    if (nargout>2), fid=0; end;
    return; 
end

% % if uigetfile don't work
% if (~ischar(filename) && numel(filename)==1)
%     disp('uigetfile error... trying other method (press ok to continue or Ctrl+C to abort).');    
%     prompt={'type Full path:',...
%         'file name'};
%     name=sprintf('File path & name to %s',ext);
%     numlines=1;
%     defaultanswer={ldir,strcat('filename.',ext)};
%     answer=inputdlg(prompt,name,numlines,defaultanswer);
% 
%     filename=answer{2}; pathname=answer{1};    
% end
% pause off

if (nargout>2)
    if (iscell(filename))
        fid=zeros(numel(filename),1);
        for i=1:numel(filename)
            fid(i)=fopen(fullfile(pathname,filename{i}));
        end
    else        
        fid=fopen(fullfile(pathname,filename));
    end
end
end
