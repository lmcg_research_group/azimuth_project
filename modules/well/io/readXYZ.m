function XYZ = readXYZ( fid )
%Read well data on XYZ format (*.xyz files).
% 
% 
% XYZ structure:
% - X  --
% - Y  --
% - Z  --
%
% SYNOPSIS:
%   XYZ = readXYZ(fid,wName,pathname);
%
% PARAMETERS:
%   fid      - integer file identifier.
%
% RETURNS:
%   XYZ     - XYZ structure used in this step to store read data.
%
% USAGE:
%   >> XYZ = readXYZ(fid,'W1B348','C:\B348\wells');
%
% SEE ALSO:
%   fgets, sscanf, struct, regexpi



% Read file until eof
i = 1;                  % counter of number of data

while ~feof(fid)
    tline=fgets(fid);                           % get line
    
    % get only numerical data on file
    [data,ndata] = sscanf(tline,'%f');
    if (ndata == 0), continue; end % there isn't numerical data on current line
    
    if (ndata == 3)
        % in format XYZ always there is 3 numerical data on line
        i=1+i;   % increase counter of data read
        wx(i)  = data(1); wy(i)=data(2); wy(i)=data(3); %#ok)-wmd(i-1)]);  %#ok
    end
end

% const. XYZ structure
XYZ  = struct( 'X' ,    wx, ...
               'Y' ,    wy, ...
               'Z' ,    wz);

end

