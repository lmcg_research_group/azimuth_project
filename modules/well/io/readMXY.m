function MXY = readMXY( fid )
%Read well data on MD DX DY format (*.mxy files).
% 
% 
% MXY structure:
% - MD --
% - X  --
% - Y  --
% - TVD--
%
% SYNOPSIS:
%   MXY = readMXY(fid,wName,pathname);
%
% PARAMETERS:
%   fid      - integer file identifier.
%
% RETURNS:
%   MXY     - MXY structure used in this step to store read data.
%
% USAGE:
%   >> MXY = readMXY(fid,'W1B348','C:\B348\wells');
%
% SEE ALSO:
%   fgets, sscanf, struct, regexpi, 



% Read file until eof
i = 0;                  % counter of number of data

while ~feof(fid)
    tline=fgets(fid);                           % get line
    % search for WELL header
    if (~isempty(regexpi(tline,'X-C')))   
        xhead = sscanf(tline,'# %*s %f %*s');
    end
    if (~isempty(regexpi(tline,'Y-C')))   
        yhead = sscanf(tline,'# %*s %f %*s');
    end
         
    
    % get only numerical data on file
    [data,ndata] = sscanf(tline,'%f');
    if (ndata == 0), continue; end % there isn't numerical data on current line
    
    if (i==0) % Special case for first line data
        % in format mxy always there is 3 numerical data on line
        i=1+i;   % increase counter of data read
        wmd(i)=data(1); wx(i)  = data(2)+xhead; %#ok
        wy(i)=data(3)+yhead; tvd = data(1); wtvd(i)=data(1);  %#ok
    else
        i=1+i;   % increase counter of data read
        wmd(i) = data(1); wx(i) = data(2)+wx(1); wy(i) = data(3)+wy(1); %#ok
        wtvd(i)= tvd+norm([wx(i)-wx(i-1),wy(i)-wy(i-1),wmd(i)-wmd(i-1)]);  %#ok
        tvd = wtvd(i);
    end
end

% const. MXY structure
MXY  = struct( 'X' ,    wx, ...
               'Y' ,    wy, ...
               'MD' ,   wmd, ...
              'TVD',  wtvd);

end

