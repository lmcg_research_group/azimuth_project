function azMarker = get_marker(number)
%Wrap function to get marker (used um plot function) related to index provided.
    for i=1:length(number)
       switch number(i)
          case 1, azMarker(i) = '+';
          case 2, azMarker(i) = 'o';
          case 3, azMarker(i) = '*';
          case 4, azMarker(i) = '.';
          case 5, azMarker(i) = 'x';
          case 6, azMarker(i) = 's';
          case 7, azMarker(i) = 'd';
          case 8, azMarker(i) = '^';
          case 9, azMarker(i) = 'v';
          case 10, azMarker(i) = '>';
          case 11, azMarker(i) = '<';
          case 12, azMarker(i) = 'p';
          case 13, azMarker(i) = 'h';
          otherwise , azMarker(i) = 'o';
       end
    end
end
