function rgb = get_rgb(colour)
%Wrap function to get rgb values related to string color provided.
	rgb = zeros(length(colour),3);
    for i=1:length(colour)
       switch lower(colour(i)),
          case {'y', 'yellow' }, rgb(i,:) = [1, 1, 0];
          case {'m', 'magenta'}, rgb(i,:) = [1, 0, 1];
          case {'c', 'cyan'   }, rgb(i,:) = [0, 1, 1];
          case {'r', 'red'    }, rgb(i,:) = [1, 0, 0];
          case {'g', 'green'  }, rgb(i,:) = [0, 1, 0];
          case {'b', 'blue'   }, rgb(i,:) = [0, 0, 1];
          case {'w', 'white'  }, rgb(i,:) = [1, 1, 1];
          case {'k', 'black'  }, rgb(i,:) = [0, 0, 0];
          otherwise            , rgb(i,:) = [0, 0, 1]; % Unknown colour -> 'blue'.
       end
    end
end
