function azCore
%Driver function of core analysis module.
%
% SYNOPSIS:
%   azCore()
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azCore()
%
% SEE ALSO:
%   menu, azCoreProject, switch, azLoadField, azCoreAnalysis
% 
%

clc; clear all;
wellcomeMsg = 'Core Analysis';

azwell=0; azField = [];
while(azwell<3)
    azwell=menu(wellcomeMsg,...
        'Project',...       % azwell == 1
        'Analysis',...      % azwell == 2
        'Exit');            % azwell == 3
    
    switch azwell
        case 1
            [azField]=azCoreProject();
        case 2
            if isempty(azField)
                azField = azLoadField('Load Core Project to start analysis');
            end
            azCoreAnalysis(azField);
        case 3
            return
    end

end

