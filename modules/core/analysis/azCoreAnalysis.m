function azCoreAnalysis(C)
%Function to drive core data analysis.
%
% SYNOPSIS:
%   azCoreAnalysis(C)
%
% PARAMETERS:
%   C   - structure used in the core analysis module to process some computation.
%         see core_structure.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azCoreAnalysis(W)
%
% SEE ALSO:
%   menu, assert, struct, azBarPlots
% 
%

%% check inputs
assert(isstruct(C),'**ERROR: Input must be a struct var type');
assert(isfield(C,'core'),'**ERROR: struct var input must has ''nWells'' field');


wellcomeMsg = 'Core Analysis Module';
azwell=0;
is2Exit = 3;


%% Drive step

% oo loop to visit each procedures
while(azwell< is2Exit)
    azwell=menu(wellcomeMsg,...
        'bar',...       % azwell == 1
        'box',...       % azwell == 2
        'Exit/Return'); % azwell == 3
    
    switch azwell
        case 1  % Bar Plots
            azBarPlots(C.core)
        case 2  % Box Plots
            azBoxPlots(C.core)
    end

end

end

