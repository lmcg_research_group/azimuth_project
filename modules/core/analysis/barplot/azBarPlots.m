function azBarPlots( C )
%Bar plots analysis.
%
% SYNOPSIS:
%   azBarPlots(C)
%
% PARAMETERS:
%   C   - structure used in the core analysis module to process some computation.
%         see core_structure.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azBarPlots(W)
%
% SEE ALSO:
%   menu, switch



%% check inputs
assert(isstruct(C),'**ERROR: Input must be a struct var type');
assert(isfield(C,'data'),'**ERROR: struct var input must has ''data'' field');


%% Analysis step

%-------------------------
%%% choose core structure
% collecting name of wells
cNames = cell(numel(C),1);
for i=1:numel(C)
    cNames{i} = C(i).origin;
end

% Open list dialog window to select core
ok = 0; %#ok % ok status: ok=1 if user click the OK button,
             %            ok=0 if user click the Cancel button or close the dialog box 

[i,ok] = listdlg('PromptString','Select core data to plot',...
                'SelectionMode','single',...
                'ListString',cNames);

% user click Cancel or Close dialog box... so we leave here
if (ok==0)
    return;
end

% get selected core data
core = C(i);
        
%--------------------------------
%%% choose category
% collecting name of categories (only cell var types)

cat = 0;
for i=1:numel(core.data)
    if iscellstr(core.data(i).var) 
        cat = cat + 1;
        cNames{cat} = core.data(i).name;
        cVar{cat} = core.data(i).var; %#ok<*AGROW>
    end
end

% Open list dialog window to select category/Group 
ok = 0; %#ok % ok status: ok=1 if user click the OK button,
             %            ok=0 if user click the Cancel button or close the dialog box 

[i,ok] = listdlg('PromptString','Select Category/Group',...
                'SelectionMode','single',...
                'ListString',cNames);
% user click Cancel or Close dialog box... so we leave here
if (ok==0); return; end

% set category
category = cVar{i};

        
%--------------------------------
%%% choose variables
% collecting name of variables

cNames = {core.data(:).name};
% cNames = cell(numel(core.data),1);
% for i=1:numel(core.data)
%     cNames{i} = core.data(i).name;
% end


% Open list dialog window to select values
ok = 0; %#ok % ok status: ok=1 if user click the OK button,
             %            ok=0 if user click the Cancel button or close the dialog box 

[idx,ok] = listdlg('PromptString','Select variable',...
                'SelectionMode','multiple',...
                'ListString',cNames);
            
% user click Cancel or Close dialog box... so we leave here
if (ok==0); return; end

% plot data
for i=1:numel(idx)
    % set var.
    var = core.data(idx(i)).var;
    % Whether var is a cellstr analysis is by incidence otherwise is mean
    if (iscellstr(var))
        [val,GN,IN] = byIncidence(category,var);
        for j=1:numel(IN)
            figure;
            bar(val(j,:))
            set(gca, 'XTickLabel',GN, 'XTick',1:numel(GN))
            title(sprintf('%s incidence',IN{j}))
        end        
    else
        [val,GN] = byMean(category,var);
        figure;
        bar(val)
        set(gca, 'XTickLabel',GN, 'XTick',1:numel(GN))
        title(sprintf('%s mean values',core.data(idx(i)).name))
    end
end




end


% -------------------------------------------------------------------------
% evaluate values by incidence
function [val,GN,IN] = byIncidence(cat,var)
% check input
assert(numel(cat)==numel(var))

[G,GN] = grp2idx(cat); % collect categories/groups and idx
[I,IN] = grp2idx(var); % collect incidences and idx
% incidence values
val = zeros(numel(IN),numel(GN));
for j=1:numel(I)
    val(I(j),G(j)) = val(I(j),G(j)) + 1;
end
end



% -------------------------------------------------------------------------
% evaluate values by mean
function [val,GN] = byMean(cat,var)
% check input
assert(numel(cat)==numel(var))

[G,GN] = grp2idx(cat); % collect categories/groups and idx
val = zeros(1,numel(GN));
for i=1:numel(GN)
    val(i) = mean(var(G==i));
end
end


