function azBoxPlots(C)
%Box plots analysis.
%
% SYNOPSIS:
%   azBoxPlots(C)
%
% PARAMETERS:
%   C   - structure used in the core analysis module to process some computation.
%         see core_structure.
%
% RETURNS:
%   None.
%
% USAGE:
%   >> azBoxPlots(W)
%
% SEE ALSO:
%   menu, switch



%% check inputs
assert(isstruct(C),'**ERROR: Input must be a struct var type');
assert(isfield(C,'data'),'**ERROR: struct var input must has ''data'' field');


%% Analysis step

%-------------------------
%%% choose core structure
% collecting name of wells
cNames = cell(numel(C),1);
for i=1:numel(C)
    cNames{i} = C(i).origin;
end

% Open list dialog window to select core
ok = 0; %#ok % ok status: ok=1 if user click the OK button,
             %            ok=0 if user click the Cancel button or close the dialog box 

[i,ok] = listdlg('PromptString','Select core data to plot',...
                'SelectionMode','single',...
                'ListString',cNames);

% user click Cancel or Close dialog box... so we leave here
if (ok==0)
    return;
end

% get selected core data
core = C(i);
        
%--------------------------------
%%% choose category
% collecting name of categories (only cell[string] var types)

cat = 0;
for i=1:numel(core.data)
    if iscellstr(core.data(i).var) 
        cat = cat + 1;
        cNames{cat} = core.data(i).name;
        cVar{cat} = core.data(i).var; %#ok<*AGROW>
    end
end

% Open list dialog window to select category/Group 
ok = 0; %#ok % ok status: ok=1 if user click the OK button,
             %            ok=0 if user click the Cancel button or close the dialog box 

[i,ok] = listdlg('PromptString','Select Category/Group',...
                'SelectionMode','single',...
                'ListString',cNames);
% user click Cancel or Close dialog box... so we leave here
if (ok==0); return; end

% set category
category = cVar{i};

        
%--------------------------------
%%% choose variables
% collecting name of variables (only float var types)

cNames = [];
cat = 0;
for i=1:numel(core.data)
    if (~iscellstr(core.data(i).var))
        cat = cat + 1;
        cNames{cat} = core.data(i).name;
        cVar{cat} = core.data(i).var; %#ok<*AGROW>
    end
end


% Open list dialog window to select values
ok = 0; %#ok % ok status: ok=1 if user click the OK button,
             %            ok=0 if user click the Cancel button or close the dialog box 

[idx,ok] = listdlg('PromptString','Select variable',...
                'SelectionMode','multiple',...
                'ListString',cNames);
            
% user click Cancel or Close dialog box... so we leave here
if (ok==0); return; end

% plot data
for i=1:numel(idx)
    figure;
    % set var.
    var = cVar{idx(i)};
    boxplot(var,category)
    title(sprintf('%s',cNames{idx(i)}))
end

end
