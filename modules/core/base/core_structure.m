%main variable structure used in the core module.
%
%   azField - representation of project variables created during some 
%             analysis. A master structure having the following fields:
%    - Name --
%        Name of core project loaded.
%
%    - core --
%        A structure specifying properties for each individual core data 
%        inported from some AnaSete file. See core below for details.
%
% 
% 
%%
%   core - core structure imported from AnaSeTe files.
%    - origin --
%        core name (name of file from which core data was imported).
% 
%    - data --
%        A structure specifying data imported from AnaSeTe file. See data
%        below for details
% 
%%
%   data - core data (list of parameters) structure imported from AnaSeTe 
%          files.
%    - name --
%        parameter name (name of profile Log or analysis parameter).
% 
%    - var --
%        Values of parameter imported (Could be a array or cell of string).
% 