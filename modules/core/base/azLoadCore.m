function [C,stat] = azLoadCore( )
%Function to load core data from file and build core structure (see core_structure.m).
% 
% SYNOPSIS:
%   [C,stat] = azLoadCore( );
%   C = azLoadCore( )
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   C     - core structure used in the core analysis module to process some 
%         computation. see core_structure.
%   stat  - An integer that identifies the status of this process. If some
%           erroneous behavior happen, stat is 1.
%
% USAGE:
%   >> C = azLoadCore( );
%
% SEE ALSO:
%   menu, struct, core_structure, azGetFile, fileparts, fprintf
% 

    % Open dialog window to select file
    [filename,~,fid,stat]=azGetFile('*.*','File export from AnaSeTe',[ ],'on');
    
    % check if file is good (see help azGetFile).
    if (stat==1), C=[]; return; end;
    
    % opened one file or multiples files
    if (iscell(filename))
        nC = numel(filename);
    else
        nC = 1; filename = {filename};
    end
    
    % loop over wells
    for iwell=1:nC
        % extract fullpath, nameOfFile and extension
        [pw,wName,e]=fileparts(filename{iwell}); %#ok 

        % Reading Data
        fprintf('*Reading file: %s ...',filename{iwell})
        % read header
        varnames = readAnaSeteHeader(fid(iwell)); vNames = varnames{1};
        
        % Select variables to be imported as string
        
        % List dialog window to select well 
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [i,ok] = listdlg('PromptString',{'Select which var. will be','imported as String'},...
                        'SelectionMode','multiple',...
                        'ListString',vNames);

        % user click Cancel or Close dialog box... so we leave here
        if (ok==0)
            stat=0; C(iwell) = []; %#ok<*AGROW>
            return;  
        end
        
        % reading variables...
        data = readAnaSeteData(fid(iwell),numel(vNames),i);
        
        % Construct core structure
        C(iwell).origin = filename{iwell};
        for i=1:numel(varnames{1})
            C(iwell).data(i).var = data{i};
            C(iwell).data(i).name = vNames{i};
        end
        
        % close access of file
        fclose(fid(iwell));
        fprintf(' Done\n')
    end
end


%--------------------------------------------------------------------------
% Read header AnaSeTe file
function [names] = readAnaSeteHeader(fid)

data = textscan(fid, '%d',1,'HeaderLines',1); nvar = data{1};
names = textscan(fid, '%s',nvar,'HeaderLines',1);

end

%--------------------------------------------------------------------------
% Read data in AnaSeTe file
function [data] = readAnaSeteData( fid, nvar, isStrings)

float = '%f '; str = '%s ';

% always first column if float = Perfil
formatsStr = '%f ';

% array containing formats
format = cell(nvar,1);
for i=1:nvar
    format{i} = float;
end
for i=1:numel(isStrings)
    format{isStrings(i)} = str;
end
% complete format to read data
for i=2:nvar
    formatsStr = strcat(formatsStr,format{i},' ');
end

data = textscan(fid,formatsStr);

end