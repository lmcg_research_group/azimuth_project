function nC = azRenameCoreData(C)
%Function to rename data from core structure (see core_structure.m).
%
% SYNOPSIS:
%   nC = azRenameCoreData(C)
%
% PARAMETERS:
%  C      - core structure used in the core analysis module to process some 
%         computation. see core_structure.
%
% RETURNS:
%   nC     - core structure modified after renamed some core data.
%
% USAGE:
%   >> C= azRenameCoreData(COld);
%
% SEE ALSO:
%   menu, struct, core_structure, listdlg, inputdlg
% 

    %% check input
    assert(isvector(C),'*ERROR: input variable must be a vector of structures');
    for icore=1:numel(C)
        assert(isstruct(C(icore)),...
            '*ERROR: all components of input must be a structure type')
    end
    
    
    %% choose core(s) structure to rename
    
    % collecting name of core
    cData = cell(numel(C),1);
    for icore=1:numel(C)
        cData{icore} = C(icore).origin;
    end
    % copy input to output
    nC = C;
    
    % List dialog window to select core
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 
	
    [icore,ok] = listdlg('PromptString','Select core to rename some data:',...
                    'SelectionMode','multiple',...
                    'ListString',cData);

	% user click Cancel or Close dialog box... so we leave here
    if (ok==0) return;  end
    
    %% rename core data
    % If we get here, we will rename some data from core(s) selected
    
    % loop over core data selected
    
    for ic=1:numel(icore)
       
        % collecting name of core
        cData = cell(numel(C(icore(ic))),1);
        for idata=1:numel(C(icore(ic)).data)
            cData{idata} = C(icore(ic)).data(idata).name;
        end 
        
        % List dialog window to select core
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [idata,ok] = listdlg('PromptString','Select data to rename:',...
                        'SelectionMode','multiple',...
                        'ListString',cData);

        % user click Cancel or Close dialog box... so we leave here
        if (ok==0) return;  end

        for id=1:numel(idata)
            prompt={'New Data Name:'};
            dlg_title='Rename Data';
            numlines=1;
            def_ans = {nC(icore(ic)).data(idata(id)).name};
            answer=inputdlg(prompt,dlg_title,numlines,def_ans);

            % Cancel procedure... so we get out here
            if isempty(answer), continue; end

            nC(icore(ic)).data(idata(id)).name = answer{1};
        end
    end
    
end


