function [azField, status] = azCoreNewProject(inproj)
%Function create core Projects on azimuth program and build core structure.
%
% SYNOPSIS:
%   [azField, status] = azCoreNewProject();
%   azField = azCoreNewProject()
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   azField - core structure used in the azimuth program. see core_structure.
%   stat  - An integer that identifies the status of this process. If some
%           erroneous behavior happen, stat is 1.
%
% USAGE:
%   >> [azField, status] = azCoreNewProject();
%   >> azField = azCoreNewProject()
%
% SEE ALSO:
%   menu, struct, core_structure, azLoadCore, azDeleteCore, azRenameCore,
%   printResume

%% Check inputs
if (nargin==1)
    wellcomeMsg = 'edit Azimuth-Core Project: building data';
    core=inproj.core;
else
    wellcomeMsg = 'new Azimuth-Core Project: building data';
    core =[];
end

%% Choose procedures
azField = [];
aznewPrj=0;
while(aznewPrj<7)
    % main menu of new project build
    aznewPrj=menu( wellcomeMsg         ,...
                   'add Core Data'     ,...  % aznewPrj =  1
                   'remove Core Data'  ,...  % aznewPrj =  2
                   'rename parameters' ,...  % aznewPrj =  3
                   'delete parameters' ,...  % aznewPrj =  4
                   'Resume Core Data'  ,...  % aznewPrj =  5
                   'Save ...'          ,...  % aznewPrj =  6
                   'Exit/Return');           % aznewPrj =  7

    switch(aznewPrj)
        %------------------------------------------------------------------
        case (1)  % add Core Data
            [C,stat]=azLoadCore();
            if (stat==0)
                if (~isempty(C))
                    core=[core, C]; %#ok<*AGROW>
                    for i=1:numel(C)
                        fprintf('*Added Core Data with %d parameters\n',...
                            numel(C(i).data));
                    end
                end
            end
            
        %------------------------------------------------------------------
        case (2)  % del Core Data
            tmpCore =azDeleteCore(core);
            clear core;
            core=tmpCore; clear tmpCore;            
            
        %------------------------------------------------------------------
        case (3)  % rename Core parameters
            tmpCore =azRenameCoreData(core);
            clear core;
            core=tmpCore; clear tmpCore;
                        
            
        %------------------------------------------------------------------
        case (4)  % remove Core parameters
            tmpCore =azDeleteCoreData(core);
            clear core;
            core=tmpCore; clear tmpCore;
            
        %------------------------------------------------------------------
        case (5)  % Resume Core Data (print project info...)
            if (nargin==1)
                printResume(core, inproj.Name)
            else
                printResume(core)
            end

        %------------------------------------------------------------------
        case (6) % Save project 
            if (~isempty(core))
                azField=azSaveField('core',core);
                printReport(azField)
            else
                azField = [];
            end
    end
end
status=0;
end



function printResume(Field,Name)

if (nargin>1)
    NameProj=Name;
else
    NameProj='Unamed';
end


fprintf('\n\n\n-------------------\n');
fprintf('Azimuth-Core Project info\n');
fprintf('Name: %s\n', NameProj);
fprintf('numberOfCoreData: %d\n',numel(Field));
fprintf('Core List:\n')

for c=1:numel(Field)
   fprintf('- core-data#%d::Imported from: %s\n',c, Field(c).origin)
   for d=1:numel(Field(c).data)
       fprintf(' -> Data%d::Name: %s\n', d, ...
           Field(c).data(d).name)
   end
end

end




function printReport(Field)
clc;
fprintf('\n\n\n-------------------\n');
fprintf('Azimuth-Core Project info\n');
fprintf('Name: %s\n',Field.Name);
fprintf('numberOfCoreData: %d\n',numel(Field));
fprintf('Core List:\n')

for c=1:numel(Field.core)
   fprintf('- core-data#%d::Imported from: %s\n',c, Field.core(c).origin)
   for d=1:numel(Field.core(c).data)
       fprintf(' -> Data%d::Name: %s\n', d, ...
           Field.core(c).data(d).name)
   end
end

end

