% Files
%   azCoreNewProject - Function create core Projects on azimuth program and build core structure.
%   azCoreProject    - Function to create/load/edit projects of core analysis module.
%   azDeleteCore     - Function to delete core data from core structure as described in core_structure.
%   azLoadCore       - Function to load core data from file and build core structure (see core_structure.m).
%   core_structure   - main variable structure used in the core module.
%   azDeleteCoreData - Function to remove data from core structure (see core_structure.m).
%   azRenameCoreData - Function to rename data from core structure (see core_structure.m).
