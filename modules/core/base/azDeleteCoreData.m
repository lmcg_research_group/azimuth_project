function nC = azDeleteCoreData(C)
%Function to remove data from core structure (see core_structure.m).
%
% SYNOPSIS:
%   nC = azDeleteCoreData(C)
%
% PARAMETERS:
%  C      - core structure used in the core analysis module to process some 
%         computation. see core_structure.
%
% RETURNS:
%   nC     - core structure modified after renamed some core data.
%
% USAGE:
%   >> C= azDeleteCoreData(COld);
%
% SEE ALSO:
%   menu, struct, core_structure, listdlg, inputdlg
% 

    %% check input
    assert(isvector(C),'*ERROR: input variable must be a vector of structures');
    for icore=1:numel(C)
        assert(isstruct(C(icore)),...
            '*ERROR: all components of input must be a structure type')
    end
    
    
    %% choose core(s) structure to rename
    
    % collecting name of core
    cData = cell(numel(C),1);
    for icore=1:numel(C)
        cData{icore} = C(icore).origin;
    end
    % copy input to output
    nC = C;
    
    % List dialog window to select core
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 
	
    [icore,ok] = listdlg('PromptString','Select core to rename some data:',...
                    'SelectionMode','multiple',...
                    'ListString',cData);

	% user click Cancel or Close dialog box... so we leave here
    if (ok==0) return;  end
    
    %% rename core data
    % If we get here, we will rename some data from core(s) selected
    
    % loop over core data selected
    
    for ic=1:numel(icore)
       
        % collecting name of core
        cData = cell(numel(C(icore(ic)).data),1); 
        idx = true(numel(C(icore(ic)).data),1);
        for idata=1:numel(C(icore(ic)).data)
            cData{idata} = C(icore(ic)).data(idata).name;
        end 
        
        % List dialog window to select core
        ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                     %            ok=0 if user click the Cancel button or close the dialog box 

        [idata,ok] = listdlg('PromptString','Select data to remove:',...
                        'SelectionMode','multiple',...
                        'ListString',cData);

        % user click Cancel or Close dialog box... so we leave here
        if (ok==0) return;  end

        % mark data to remove
        idx(idata) = false;
        copydata = nC(icore(ic)).data;
        nC(icore(ic)).data = copydata(idx);
        clear copydata
    end
    
end


