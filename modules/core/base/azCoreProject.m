function proj = azCoreProject
%Function to create/load/edit projects of core analysis module.
%
% SYNOPSIS:
%   prj = azCoreProject()
%
% PARAMETERS:
%   None.
%
% RETURNS:
%   prj  - project variable, containing core strucrure;
%
% USAGE:
%   >> coreProj = azCoreProject()
%
% SEE ALSO:
%   menu, switch, azCoreNewProject, azLoadField
% 
%

clc; clear all;
wellcomeMsg = 'Core Analysis Project Builder';

azwell=0; proj = [];
while(azwell<4)
    azwell=menu(wellcomeMsg,...
        'New Project',...       % azwell == 1
        'Load Project',...      % azwell == 2
        'Edit Project',...      % azwell == 3
        'Exit/Return');         % azwell == 4
    
    switch azwell
        case 1 % new project creation
            proj = azCoreNewProject();
        case 2 % load project
            proj = azLoadField('Load Core Project');
        case 3 % edit project 
            if isempty(proj)
                proj = azLoadField('Load Core Project to edit');
            end
            proj = azCoreNewProject(proj);
        case 4
            return
    end

end

