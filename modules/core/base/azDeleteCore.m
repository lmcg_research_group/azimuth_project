function nC = azDeleteCore(C)
%Function to delete core data from core structure as described in core_structure.
%
% SYNOPSIS:
%   nC = azDeleteCore(C)
%
% PARAMETERS:
%  C      - core structure used in the core analysis module to process some 
%         computation. see core_structure.
%
% RETURNS:
%   nC    - core structure modified after delete some core.
%
% USAGE:
%   >> C= azDeleteCore(COld);
%
% SEE ALSO:
%   menu, struct, core_structure, assert, listdlg, 
% 

    %% check input
    assert(isvector(C),'*ERROR: input variable must be a vector of structures');
    for icore=1:numel(C)
        assert(isstruct(C(icore)),...
            '*ERROR: all components of input must be a structure type')
    end
    
    
    %% choose core data(s) structure to delete
    
    % collecting name of core data
    cNames = cell(numel(C),1);
    for icore=1:numel(C)
        cNames{icore} = C(icore).origin;
    end
    
    % List dialog window to select core 
    ok = 0; %#ok % ok status: ok=1 if user click the OK button,
                 %            ok=0 if user click the Cancel button or close the dialog box 
	
    [icore,ok] = listdlg('PromptString','Select core to remove from Project:',...
                    'SelectionMode','multiple',...
                    'ListString',cNames);

	% user click Cancel or Close dialog box... so we leave here
    if (ok==0)
        nC = C;
        return; 
    end
    
    %% delete well list data
    % If we get here, we will delete some core
        
    % mark wells to remove
    idx=ones(1,numel(C)); idx(icore)=0;
    ic=0;
    for i=1:numel(C)
        if (idx(i))
            ic=ic+1; 
            nC(ic) = C(i); %#ok<*AGROW>
        end
    end
    
end


